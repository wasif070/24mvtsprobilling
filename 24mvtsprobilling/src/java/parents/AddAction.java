package parents;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import org.apache.log4j.Logger;

public class AddAction extends Action {

    static Logger logger = Logger.getLogger(AddAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            ParentForm formBean = (ParentForm) form;
            ParentDTO dto = new ParentDTO();
            ParentTaskSchedular scheduler = new ParentTaskSchedular();

            dto.setParent_name(formBean.getParent_name());
            dto.setParent_desc(formBean.getParent_desc());

            MyAppError error = scheduler.addParent(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getParentDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("parent");
                ac_dto.setPrimaryKey(dto.getParent_name());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
                
                formBean.setMessage(false, "Parent is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());                
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
