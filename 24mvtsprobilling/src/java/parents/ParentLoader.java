package parents;

import com.myapp.struts.util.AppConstants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class ParentLoader {

    static Logger logger = Logger.getLogger(ParentLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private ArrayList<ParentDTO> parentList = null;
    private HashMap<Long, ParentDTO> parentDTOByID = null;
    static ParentLoader parentLoader = null;

    public ParentLoader() {
        forceReload();
    }

    public static ParentLoader getInstance() {
        if (parentLoader == null) {
            createParentLoader();
        }
        return parentLoader;
    }

    private synchronized static void createParentLoader() {
        if (parentLoader == null) {
            parentLoader = new ParentLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            parentDTOByID = new HashMap<Long, ParentDTO>();
            parentList = new ArrayList<ParentDTO>();
            String sql = "select id,parent_name,parent_desc,status from parents where is_deleted=0 order by id DESC ";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                ParentDTO dto = new ParentDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setParent_name(resultSet.getString("parent_name"));
                dto.setParent_desc(resultSet.getString("parent_desc"));
                dto.setStatusStr(resultSet.getInt("status") == 1 ? "Active" : "Inactive");
                parentDTOByID.put(dto.getId(), dto);
                parentList.add(dto);
            }
            resultSet.close();

        } catch (Exception e) {
            logger.fatal("Exception in ParentLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<ParentDTO> getParentDTOList() {
        checkForReload();
        logger.debug("parent dto list size-->"+parentList.size());
        return parentList;
    }

    public synchronized ParentDTO getParentDTOByID(long id) {
        checkForReload();
        return parentDTOByID.get(id);
    }

    public ArrayList<ParentDTO> getParentDTOsWithSearchParam(ParentDTO p_dto) {
        logger.debug("parent dto list size getParentDTOsWithSearchParam-->"+parentList.size());
        ArrayList newList = null;
        checkForReload();
        ArrayList<ParentDTO> list = this.parentList;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ParentDTO dto = (ParentDTO) i.next();
                if ((p_dto.sw_name && !dto.getParent_name().toLowerCase().startsWith(p_dto.getParent_name()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<ParentDTO> getParentDTOsSorted() {
        checkForReload();
        ArrayList<ParentDTO> list = this.parentList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ParentDTO dto1 = (ParentDTO) o1;
                    ParentDTO dto2 = (ParentDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
