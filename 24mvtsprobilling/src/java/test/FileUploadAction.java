package test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class FileUploadAction extends Action {

    static Logger logger = Logger.getLogger(FileUploadAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        try {

            TestForm fileUploadForm = (TestForm) form;

            //FormFile file = fileUploadForm.getFile();
            List<FormFile> formFiles = fileUploadForm.getFormFiles();

            //Get the servers upload directory real path name
            String filePath = getServlet().getServletContext().getRealPath("/") + "upload_test";

            //create the upload folder if not exists
            File folder = new File(filePath);
            if (!folder.exists()) {
                folder.mkdir();
            }
            logger.debug("form file size: "+formFiles.size());

            for (FormFile file : fileUploadForm.getFormFiles()) {
                String fileName = file.getFileName();

                if (!("").equals(fileName)) {

                    logger.debug("Server path:" + filePath);
                    File newFile = new File(filePath, fileName);

                    if (!newFile.exists()) {
                        FileOutputStream fos = new FileOutputStream(newFile);
                        fos.write(file.getFileData());
                        fos.flush();
                        fos.close();
                    }

                    logger.debug("uploadedFilePath" + newFile.getAbsoluteFile());
                    logger.debug("uploadedFileName" + newFile.getName());
                }

            }
        } catch (Exception ex) {
            logger.debug("Exception file uploading-->" + ex);
        }


        return mapping.findForward("success");
    }
}