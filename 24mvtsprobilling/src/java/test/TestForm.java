package test;

import java.util.ArrayList;
import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class TestForm extends ActionForm {

    private FormFile file;
    private String[] upload;
    private List<FormFile> formFiles = new ArrayList<FormFile>();

    public String[] getUpload() {
        return upload;
    }

    public void setUpload(String[] upload) {
        this.upload = upload;
    }

//
//    @Override
//    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
//
//        ActionErrors errors = new ActionErrors();
//
//        if (getFile().getFileSize() == 0) {
//            errors.add("common.file.err", new ActionMessage("error.common.file.required"));
//            return errors;
//        }
//
//        if (!"text/plain".equals(getFile().getContentType())) {
//            errors.add("common.file.err.ext", new ActionMessage("error.common.file.textfile.only"));
//            return errors;
//        }
//
//        if (getFile().getFileSize() > 10240) { //10kb
//            errors.add("common.file.err.size", new ActionMessage("error.common.file.size.limit", 10240));
//            return errors;
//        }
//
//        return errors;
//    }
    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }

    public List<FormFile> getFormFiles() {
        return formFiles;
    }

    public void setFormFiles(FormFile formFiles) {
        this.formFiles.add(formFiles);
    }
}