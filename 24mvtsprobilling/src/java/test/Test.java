/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.myapp.struts.util.Utils;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class Test {

    public static String getDateDDMMYYhhmmss(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static double calculateBill(int duration, RateDTO rdto) {
        double bill = 0;
        int next_sec = 0;

        if (duration <= rdto.getRateFirstPulse()) {
            duration = rdto.getRateFirstPulse();
        }
        next_sec = duration % 60;
        duration = duration - next_sec;
        if (next_sec <= rdto.getRateNextPulse()) {
            next_sec = rdto.getRateNextPulse();
        }
        duration = duration + next_sec;

        if (duration <= rdto.getRateFailedPeriod()) {
            duration = 0;
        }

        if (duration > rdto.getRateGracePeriod()) {
            duration = duration - rdto.getRateGracePeriod();
        } else {
            duration = 0;
        }
        bill = (duration * rdto.getRatePerMin()) / 60;
        //long l = (long) (bill * BillGenerator.precesion);
        //double finalBill = (double) l / BillGenerator.precesion;
        return bill;
    }

    public static ArrayList<Integer> findSubstring(String S, String[] L) {
        if (S == null || L == null || L.length == 0 || S.length() == 0) {
            return null;
        }

//        ArrayList<Integer> indices = new ArrayList<Integer>();
//        ConcurrentHashMap<String, Integer> str_param = new ConcurrentHashMap<String, Integer>();
//        for (int i = 0; i < L.length; i++) {
//            str_param.put(L[i], i);
//        }
//
//        int pointer = 0;
//
//        for (int i = pointer; i < S.length(); i++) {
//
//            for (String str_key : str_param.keySet()) {
//                if (S.startsWith(str_key, i)) {
//                    indices.add(i);
//                    str_param.remove(str_key);
//                    pointer = i + str_key.length() + 1;
//                    i = pointer;
//                }
//            }
//        }


        ArrayList<Integer> indices = new ArrayList<Integer>();

        int pointer = 0;

        for (int i = pointer; i < S.length(); i++) {
            for (int j = 0; j < L.length; j++) {
                if (L[j] != null && S.startsWith(L[j], i)) {
                    indices.add(i);
                    pointer = i + L[j].length() + 1;
                    i = pointer;
                    L[j] = null;
                }
            }
        }


        return indices;
    }

    public static ArrayList<Integer> findSubstring1(String S, String[] L) {
        if (S == null || L == null || L.length == 0 || S.length() == 0) {
            return null;
        }

        ArrayList<Integer> indices;
        HashMap<String, Integer> map = new HashMap<String, Integer>();

        int wLength = L[0].length();
        int strL = S.length();

        if (strL % wLength == 0) {
            for (int i = 0; i < strL; i += wLength) {
                int first = 0;
                String subStr = S.substring(i, i + wLength);

                while (first < L.length) {
                    if (subStr.equals(L[first])) {

                        map.put(subStr, i);

                        break;
                    }
                    first++;
                }
            }
        }
        indices = new ArrayList<Integer>(map.values());
        return indices;
    }

    public static void main(String[] args) {

        String S = "lingmindraboofooowingdingbarrwingmonkeypoundcake";
        String[] L = {"fooo", "barr", "wing", "ding", "wing"};


        System.out.println("-->" + findSubstring(S, L));
        
        /* RateDTO rdto = new RateDTO();
         calculateBill(130, rdto);*/

        /* System.out.println(Utils.ToDateDDMMYYYYhhmmss(1350184726000L));
         System.exit(0);
         String p_url = "http://38.127.68.245/get_opcode_settings.php?cmd=update&pin=510";

         String content = "";
         try {
         URL url = new URL(p_url);
         URLConnection urlConnection = url.openConnection();


         BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
         String inputLine;
         while ((inputLine = in.readLine()) != null) {
         content += inputLine;
         }


         } catch (Exception e) {
         }
         System.out.println(content);
         JsonParser parser = new JsonParser();
         JsonObject o = (JsonObject) parser.parse(content);
         System.out.println(o.get("Reason"));*/

        /*class A {

         transient int a = 1;
         int b = 2;
         }
         A x = new A();
         System.out.println("" + x.a);
         System.out.println("" + x.b);
         int i = 0;
         int a = i++ + ++i;
         System.out.println("i++ = " + i++ + ++i);
         System.out.println("" + a);*/



    }
}
