/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import databaseconnector.*;
import com.myapp.struts.util.AppError;

/**
 *
 * @author Administrator
 */
public class ActivityDBFunction {

    static Logger logger = Logger.getLogger(ActivityDBFunction.class.getName());

    public static AppError addActivityLog(ActivityDTO p_dto) {
        AppError error = new AppError();

        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            if (p_dto != null) {
                String sql = "insert into activity_log(user_id,log_time,changed_value,table_name,action_name,primary_key) ";
                sql += "values(?,?,?,?,?,?)";
                ps = db.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getUserId());
                ps.setLong(2, System.currentTimeMillis());
                ps.setString(3, p_dto.getChangedValue());
                ps.setString(4, p_dto.getTableName());
                ps.setString(5, p_dto.getActionName());
                ps.setString(6, p_dto.getPrimaryKey());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            error.setErrorType(error.DB_ERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Exception during update addActivityLog info-->" + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public static AppError addActivityLog(ArrayList<ActivityDTO> data) {
        AppError error = new AppError();

        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            ActivityDTO act_dto = new ActivityDTO();
            if (data != null && data.size() > 0) {
                String sql = "insert into activity_log(user_id,log_time,changed_value,table_name,action_name,primary_key) values ";
                for (int i = 0; i < data.size(); i++) {
                    act_dto = data.get(i);
                    sql += "('" + act_dto.getUserId() + "'," + System.currentTimeMillis() + "," + act_dto.getChangedValue() + "','" + act_dto.getTableName() + "','" + act_dto.getActionName() + "'," + act_dto.getPrimaryKey() + "'),";
                }
                ps = db.connection.prepareStatement(sql.substring(0, sql.length() - 1));
                ps.executeUpdate();
            }
        } catch (Exception e) {
            error.setErrorType(error.DB_ERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Exception during update addActivityLog info-->" + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public static HashMap<String, Integer> countActivity() {
        long befor24hours = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select count(*) as counter,table_name,action_name from activity_log where status=1 and is_deleted=0 and log_time>" + befor24hours + " group by table_name,action_name ";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                map.put(rs.getString("table_name") + "," + rs.getString("action_name"), rs.getInt("counter"));
            }
        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return map;
    }

    public static ArrayList<String> tableNames() {
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList<String> names = new ArrayList<String>();
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select distinct(table_name) from activity_log ";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                names.add(rs.getString("table_name"));
            }
        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return names;
    }

    public static ArrayList<String> getActions() {
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList<String> names = new ArrayList<String>();
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select distinct(action_name) as act_name from activity_log ";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                names.add(rs.getString("act_name"));
            }
        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return names;
    }

    public AppError deleteActivityLog(String ids) {
        AppError error = new AppError();

        DBConnection db = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update activity_log set is_deleted=1 where id in (" + ids + ")";
            ps = db.connection.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception e) {
            error.setErrorType(error.DB_ERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Exception during deleteActivityLog-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
