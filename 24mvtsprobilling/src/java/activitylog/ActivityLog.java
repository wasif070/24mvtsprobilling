/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;

import com.myapp.struts.clients.ClientDTO;
import java.lang.reflect.Method;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Ashraful
 */
public class ActivityLog {

    static Logger logger = Logger.getLogger(ActivityLog.class.getName());

    public ActivityLog() {
    }

    public static ArrayList<ComparisonDTO> getActivityLog(Object prevObj, Object newObj) {
        ArrayList<ComparisonDTO> list = new ArrayList<ComparisonDTO>();
        try {
            Class cls1 = Object.class.forName(prevObj.getClass().getName());
            Class cls2 = Object.class.forName(newObj.getClass().getName());
            Method methods1[] = cls1.getDeclaredMethods();

            for (int i = 0; i < methods1.length; i++) {
                String method = methods1[i].getName();
                if (method.startsWith("get")) {
                    Method method1 = cls1.getMethod(method);
                    Method method2 = cls2.getMethod(method);
                    String value1 = String.valueOf(method1.invoke(prevObj)).trim();
                    String value2 = String.valueOf(method2.invoke(newObj)).trim();
                    if (!(value1.equals(value2))) {
                        ComparisonDTO dto = new ComparisonDTO();
                        dto.setFieldName(method.substring(3));
                        dto.setPreviousValue(value1.equals("null") ? "NULL" : value1);
                        dto.setNewValue(value2.equals("null") ? "NULL" : value2);
                        list.add(dto);
                    }

                }
            }
        } catch (Exception ex) {
            logger.debug("Class not found exception--->" + ex);
        }
        return list;
    }

    public static void main(String args[]) {
        ClientDTO prevObj = new ClientDTO();
        prevObj.setId(1);
        prevObj.setClient_email("ashraful");
        prevObj.setClient_name("Rosevvv");

        ClientDTO newObj = new ClientDTO();
        newObj.setId(2);
        newObj.setClient_email("a");
        newObj.setClient_name("Rose");

        ArrayList<ComparisonDTO> list = ActivityLog.getActivityLog((Object) prevObj, (Object) newObj);
        for (ComparisonDTO dto : list) {
            System.out.print(dto.getFieldName() + ":" + dto.getPreviousValue());
            System.out.println("-->" + dto.getNewValue());
        }
    }
}
