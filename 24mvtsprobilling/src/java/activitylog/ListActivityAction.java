/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;


import org.apache.log4j.Logger;

public class ListActivityAction extends Action {

    static Logger logger = Logger.getLogger(ListActivityAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.debug("ListActivityAction class started");

        String target = AppConstants.SUCCESS;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            ActivityTaskScheduler scheduler = new ActivityTaskScheduler();

            ActivityForm actForm = (ActivityForm) form;

            /*--------------------For geting searching parameter-------------------*/
            ActivityDTO act_dto = new ActivityDTO();
            if (request.getParameter("tableName") != null && request.getParameter("tableName").length() > 0) {
                act_dto.swTableName = true;
                act_dto.setTableName(request.getParameter("tableName").trim().toLowerCase());
            }
            if (request.getParameter("primaryKey") != null && request.getParameter("primaryKey").length() > 0) {
                act_dto.swPKey = true;
                act_dto.setPrimaryKey(request.getParameter("primaryKey").trim().toLowerCase());
            }
            if (request.getParameter("userId") != null && request.getParameter("userId").length() > 0) {
                act_dto.swUserId = true;
                act_dto.setUserId(request.getParameter("userId").trim().toLowerCase());
            }
            if (request.getParameter("actionName") != null && request.getParameter("actionName").length() > 0) {
                act_dto.swActionName = true;
                act_dto.setActionName(request.getParameter("actionName").trim().toLowerCase());
            }

            /*------------------- end of geting searching parameter----------------*/

            if (actForm.getDoSearch() == null) {
                act_dto.setFromTime(System.currentTimeMillis() - AppConstants.ACTIVITY_LOG_DEFAULT_SEARCH_DURATION);
                act_dto.setToTime(System.currentTimeMillis());
                actForm.setActivityDTOs(scheduler.getSearchedActivityDTOs(act_dto));
            } else {
                act_dto.swTimeDuration = true;
                act_dto.setFromTime(Utils.ToLong(actForm.getFromDay() + "/" + actForm.getFromMonth() + "/" + actForm.getFromYear()));
                act_dto.setToTime(Utils.ToLong(actForm.getToDay() + "/" + actForm.getToMonth() + "/" + actForm.getToYear()));
                actForm.setActivityDTOs(scheduler.getSearchedActivityDTOs(act_dto));
                actForm.setDoSearch(null);
            }

            request.getSession(true).setAttribute(mapping.getAttribute(), actForm);
            request.getSession(true).setAttribute(AppConstants.ACTIVITY_LIST, actForm.getActivityDTOs());
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
