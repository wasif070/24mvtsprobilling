/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.AppConstants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class RoleListAction extends Action {

    static Logger logger = Logger.getLogger(RoleListAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.debug("RoleListAction class started");

        String target = AppConstants.SUCCESS;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            RoleTaskScheduler scheduler = new RoleTaskScheduler();
            request.getSession(true).setAttribute(AppConstants.ROLE_LIST, scheduler.getRoleDTOs());

            RoleForm roleForm = (RoleForm) form;

            /*--------------------For geting searching parameter-------------------*/
            RoleDTO userDTO = new RoleDTO();
            if (request.getParameter("roleName") != null && request.getParameter("roleName").length() > 0) {
                userDTO.swRoleName = true;
                userDTO.setRoleName(request.getParameter("roleName").trim().toLowerCase());
            }
            if (request.getParameter("roleDesc") != null && request.getParameter("roleDesc").length() > 0) {
                userDTO.swRoleDesc = true;
                userDTO.setRoleDesc(request.getParameter("roleDesc").trim().toLowerCase());
            }

            /*------------------- end of geting searching parameter----------------*/

            if (roleForm.getDoSearch() == null) {
                roleForm.setRoleDTOs(scheduler.getRoleDTOs());
            } else {
                roleForm.setRoleDTOs(scheduler.getSearchedRoleDTOs(userDTO));
            }
            logger.debug("List Size-->"+roleForm.getRoleDTOs().size());

            request.getSession(true).setAttribute(mapping.getAttribute(), roleForm);

        } else {
            return (new ActionForward("/login/logout.do", true)); 
        }
        return (mapping.findForward(target));
    }
}
