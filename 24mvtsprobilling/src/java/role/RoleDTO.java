/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class RoleDTO {

    public RoleDTO() {
    }
    private long id;
    private String roleName;
    private int pageId;
    private int status;
    private int permission;
    private ArrayList roleDTOs;
    private String roleDesc;
    private long editId;
    public boolean swRoleName = false;
    public boolean swRoleDesc = false;
    ArrayList<PermissionDTO> permissionDTOs;
    private int isDeleted;

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public long getEditId() {
        return editId;
    }

    public void setEditId(long editId) {
        this.editId = editId;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public ArrayList getRoleDTOs() {
        return roleDTOs;
    }

    public void setRoleDTOs(ArrayList roleDTOs) {
        this.roleDTOs = roleDTOs;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    public ArrayList<PermissionDTO> getPermissionDTOs() {
        return permissionDTOs;
    }

    public void setPermissionDTOs(ArrayList<PermissionDTO> permissionDTOs) {
        this.permissionDTOs = permissionDTOs;
    }
}
