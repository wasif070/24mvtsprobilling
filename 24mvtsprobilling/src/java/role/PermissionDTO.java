/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

/**
 *
 * @author Administrator
 */
public class PermissionDTO {

    public PermissionDTO() {
    }
    private long id;
    private String roleName;
    private int pageId;
    private String pageUri;
    private int permissions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public int getPermissions() {
        return permissions;
    }

    public void setPermissions(int permissions) {
        this.permissions = permissions;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPageUri() {
        return pageUri;
    }

    public void setPageUri(String pageUri) {
        this.pageUri = pageUri;
    }
}
