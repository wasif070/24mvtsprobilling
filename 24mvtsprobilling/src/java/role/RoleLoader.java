/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.Sorter;
import com.myapp.struts.util.Utils;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import databaseconnector.DBConnection;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class RoleLoader {

    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private HashMap<Long, RoleDTO> roleDTOs;
    private HashMap<Integer, PageDTO> pageDTOs;
    private HashMap<Long, HashMap<String, Integer>> permissionMap;
    static RoleLoader roleLoader = null;
    static Logger logger = Logger.getLogger(RoleLoader.class.getName());

    public RoleLoader() {
        forceReload();
    }

    public static RoleLoader getInstance() {
        if (roleLoader == null) {
            createRoleLoader();
        }
        return roleLoader;
    }

    private synchronized static void createRoleLoader() {
        if (roleLoader == null) {
            roleLoader = new RoleLoader();
        }
    }

    private void reload() {
        permissionMap = new HashMap<Long, HashMap<String, Integer>>();
        roleDTOs = new HashMap<Long, RoleDTO>();
        pageDTOs = new HashMap<Integer, PageDTO>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select * from roles where  is_deleted=0 ";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RoleDTO roleDTO = new RoleDTO();
                roleDTO.setId(rs.getLong("id"));
                roleDTO.setRoleName(rs.getString("role_name"));
                roleDTO.setRoleDesc(rs.getString("role_desc"));
                roleDTO.setStatus(rs.getInt("status"));
                roleDTO.setEditId(roleDTO.getId());
                roleDTOs.put(roleDTO.getId(), roleDTO);
            }

            RoleDBFunction dbFunc = new RoleDBFunction();
            pageDTOs = dbFunc.getPagesMap();
            permissionMap = dbFunc.getPermissionMap();

        } catch (Exception e) {
            logger.debug("Exception getting role data-->" + e);
            System.out.println(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized RoleDTO getRoleDTO(long id) {
        checkForReload();
        if (roleDTOs.containsKey(id)) {
            return roleDTOs.get(id);
        }
        return null;
    }

    public synchronized PageDTO getPageDTO(int id) {
        checkForReload();
        if (pageDTOs.containsKey(id)) {
            return pageDTOs.get(id);
        }
        return null;
    }

    public synchronized ArrayList<RoleDTO> getRoleDTOs() {
        ArrayList<RoleDTO> data = new ArrayList<RoleDTO>();
        checkForReload();
        Set set = roleDTOs.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            RoleDTO dto = (RoleDTO) me.getValue();
            data.add(dto);
        }
        data = Sorter.sortArrayListDESC(data, "getId");
        return data;
    }

    public ArrayList<RoleDTO> getRoleDTOsWithSearchParam(RoleDTO role_dto) {
        checkForReload();
        ArrayList<RoleDTO> list = getRoleDTOs();
        ArrayList newList = null;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RoleDTO dto = (RoleDTO) i.next();
                if ((role_dto.swRoleName && !dto.getRoleName().toLowerCase().startsWith(role_dto.getRoleName())) || (role_dto.swRoleDesc && !dto.getRoleDesc().toLowerCase().startsWith(role_dto.getRoleDesc()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public synchronized int[] getPermissions(long role_id, String page_uri) {
        if (role_id <= 0) {
            int[] permissions = new int[32];
            for (int i = 0; i < permissions.length; i++) {
                permissions[i] = 1;
            }
            return permissions;
        }
        checkForReload();
        RoleDTO role_dto = roleDTOs.get(role_id);
        if (role_dto == null || role_dto.getStatus() == 0) {
            int[] permissions = new int[32];
            for (int i = 0; i < permissions.length; i++) {
                permissions[i] = 0;
            }
            permissions[0] = 1;
            return permissions;
        }
        HashMap<String, Integer> map = permissionMap.get(role_id);
        if (map != null && map.size() > 0) {
            if (map.get(page_uri) != null) {
                int permissions = map.get(page_uri);
                return Utils.getBinaryArray(permissions);
            }
        }
        return new int[32];
    }

    public synchronized int[] getPermissionsForEditPage(long role_id, String page_uri) {
        if (role_id <= 0) {
            int[] permissions = new int[32];
            for (int i = 0; i < permissions.length; i++) {
                permissions[i] = 1;
            }
            return permissions;
        }
        checkForReload();
        HashMap<String, Integer> map = permissionMap.get(role_id);
        if (map != null && map.size() > 0) {
            if (map.get(page_uri) != null) {
                int permissions = map.get(page_uri);
                return Utils.getBinaryArray(permissions);
            }
        }
        return new int[32];
    }
}
