
package pagination;

public class Pagination {

    private String baseUrl; // The page we are linking to
    private int totalRows; // Total number of items (database results)
    private int perPage; // Max number of items you want shown per page
    private int numLinks; // Number of "digit" links to show before/after the currently viewed page
    private int curPage;// The current page being viewed
    private String firstLink;
    private String nextLink;
    private String prevLink;
    private String lastLink;
    private int uriSegment;
    private String fullTagOpen;
    private String fullTagClose;
    private String firstTagOpen;
    private String firstTagClose;
    private String lastTagOpen;
    private String lastTagClose;
    private String curTagOpen;
    private String curTagClose;
    private String nextTagOpen;
    private String nextTagClose;
    private String prevTagOpen;
    private String prevTagClose;
    private String numTagOpen;
    private String numTagClose;

    public Pagination() {
        this.baseUrl = "";
        this.totalRows = 0;
        this.perPage = 100;
        this.numLinks = 2;
        this.curPage = 1;
        this.firstLink = "&lsaquo; First";
        this.nextLink = "Next&rsaquo;&rsaquo;";
        this.prevLink = "&lsaquo;&lsaquo;Prev";
        this.lastLink = "Last &rsaquo;";
        this.uriSegment = 3;
        this.fullTagOpen = "";
        this.fullTagClose = "";
        this.firstTagOpen = "";
        this.firstTagClose = "&nbsp;";
        this.lastTagOpen = "&nbsp;";
        this.lastTagClose = "";
        this.curTagOpen = "&nbsp;<strong>";
        this.curTagClose = "</strong>&nbsp;";
        this.nextTagOpen = "&nbsp;";
        this.nextTagClose = "&nbsp;";
        this.prevTagOpen = "&nbsp;";
        this.prevTagClose = "&nbsp;";
        this.numTagOpen = "&nbsp;";
        this.numTagClose = "";
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public String getCurTagClose() {
        return curTagClose;
    }

    public void setCurTagClose(String curTagClose) {
        this.curTagClose = curTagClose;
    }

    public String getCurTagOpen() {
        return curTagOpen;
    }

    public void setCurTagOpen(String curTagOpen) {
        this.curTagOpen = curTagOpen;
    }

    public String getFirstLink() {
        return firstLink;
    }

    public void setFirstLink(String firstLink) {
        this.firstLink = firstLink;
    }

    public String getFirstTagClose() {
        return firstTagClose;
    }

    public void setFirstTagClose(String firstTagClose) {
        this.firstTagClose = firstTagClose;
    }

    public String getFirstTagOpen() {
        return firstTagOpen;
    }

    public void setFirstTagOpen(String firstTagOpen) {
        this.firstTagOpen = firstTagOpen;
    }

    public String getFullTagClose() {
        return fullTagClose;
    }

    public void setFullTagClose(String fullTagClose) {
        this.fullTagClose = fullTagClose;
    }

    public String getFullTagOpen() {
        return fullTagOpen;
    }

    public void setFullTagOpen(String fullTagOpen) {
        this.fullTagOpen = fullTagOpen;
    }

    public String getLastLink() {
        return lastLink;
    }

    public void setLastLink(String lastLink) {
        this.lastLink = lastLink;
    }

    public String getLastTagClose() {
        return lastTagClose;
    }

    public void setLastTagClose(String lastTagClose) {
        this.lastTagClose = lastTagClose;
    }

    public String getLastTagOpen() {
        return lastTagOpen;
    }

    public void setLastTagOpen(String lastTagOpen) {
        this.lastTagOpen = lastTagOpen;
    }

    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

    public String getNextTagClose() {
        return nextTagClose;
    }

    public void setNextTagClose(String nextTagClose) {
        this.nextTagClose = nextTagClose;
    }

    public String getNextTagOpen() {
        return nextTagOpen;
    }

    public void setNextTagOpen(String nextTagOpen) {
        this.nextTagOpen = nextTagOpen;
    }

    public int getNumLinks() {
        return numLinks;
    }

    public void setNumLinks(int numLinks) {
        this.numLinks = numLinks;
    }

    public String getNumTagClose() {
        return numTagClose;
    }

    public void setNumTagClose(String numTagClose) {
        this.numTagClose = numTagClose;
    }

    public String getNumTagOpen() {
        return numTagOpen;
    }

    public void setNumTagOpen(String numTagOpen) {
        this.numTagOpen = numTagOpen;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getPrevLink() {
        return prevLink;
    }

    public void setPrevLink(String prevLink) {
        this.prevLink = prevLink;
    }

    public String getPrevTagClose() {
        return prevTagClose;
    }

    public void setPrevTagClose(String prevTagClose) {
        this.prevTagClose = prevTagClose;
    }

    public String getPrevTagOpen() {
        return prevTagOpen;
    }

    public void setPrevTagOpen(String prevTagOpen) {
        this.prevTagOpen = prevTagOpen;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getUriSegment() {
        return uriSegment;
    }

    public void setUriSegment(int uriSegment) {
        this.uriSegment = uriSegment;
    }
}
