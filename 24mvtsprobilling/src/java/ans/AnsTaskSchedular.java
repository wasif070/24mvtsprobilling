package ans;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class AnsTaskSchedular {

    public AnsTaskSchedular() {
    }

    public MyAppError addAnsInformation(AnsDTO p_dto) {
        AnsDAO userDAO = new AnsDAO();
        return userDAO.addAnsInformation(p_dto);
    }

    public MyAppError editAnsInformation(AnsDTO p_dto) {
        AnsDAO userDAO = new AnsDAO();
        return userDAO.editAnsInformation(p_dto);
    }

    public MyAppError deleteAns(int cid) {
        AnsDAO dao = new AnsDAO();
        long[] ids = new long[1];
        ids[0] = cid;
        return dao.multipleANSDelete(ids);
    }

    public AnsDTO getAnsDTO(long id) {
        return AnsLoader.getInstance().getAnsDTOByID(id);
    }

    public ArrayList<AnsDTO> getAnsDTOsSorted(LoginDTO l_dto) {
        return AnsLoader.getInstance().getAnsDTOsSorted();
    }

    public ArrayList<AnsDTO> getAnsDTOs(LoginDTO l_dto) {
        return AnsLoader.getInstance().getAnsDTOList(l_dto);
    }

    public ArrayList<AnsDTO> getAnsDTOsWithSearchParam(AnsDTO udto, LoginDTO l_dto) {
        return AnsLoader.getInstance().getAnsDTOsWithSearchParam(udto, l_dto);
    }

    public MyAppError activateMultiple(long ansIds[]) {
        AnsDAO dao = new AnsDAO();
        return dao.multipleANSStatusUpdate(ansIds, Constants.LIVE_ACTIVE);
    }

    public MyAppError inactivateMultiple(long ansIds[]) {
        AnsDAO dao = new AnsDAO();
        return dao.multipleANSStatusUpdate(ansIds, Constants.LIVE_INACTIVE);
    }

    public MyAppError deleteMultiple(long ansIds[]) {
        AnsDAO dao = new AnsDAO();
        return dao.multipleANSDelete(ansIds);
    }
}
