package ans;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class AnsDAO {

    static Logger logger = Logger.getLogger(AnsDAO.class.getName());

    public AnsDAO() {
    }

    public MyAppError addAnsInformation(AnsDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        PreparedStatement remotePS = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select primary_prefix from ans_clients where primary_prefix='" + p_dto.getPrimary_prefix() + "' and deleted=0";
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate ANS");
                return error;
            }

            sql = "insert into ans_clients(name,company_name,primary_prefix,other_prefixes) values(?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, p_dto.getName());
            ps.setString(2, p_dto.getCompany_name());
            ps.setString(3, p_dto.getPrimary_prefix());
            ps.setString(4, p_dto.getOther_prefixes());
            ps.executeUpdate();
            resultSet = ps.getGeneratedKeys();
            if (resultSet != null && resultSet.next()) {
                error.setErrorMessage(String.valueOf(resultSet.getLong(1)));
            }
            AnsLoader.getInstance().forceReload();
            
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error in MVTSPRO.");
            logger.fatal("Error while adding ans: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remotePS != null) {
                    remotePS.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }

            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editAnsInformation(AnsDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        PreparedStatement remotePS = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select primary_prefix from ans_clients where deleted=0 and primary_prefix='" + p_dto.getPrimary_prefix() + "' and id!=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate ANS");
                return error;
            }

            sql = "update ans_clients set name=?,company_name=?,primary_prefix=?,other_prefixes=?,status=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getName());
            ps.setString(2, p_dto.getCompany_name());
            ps.setString(3, p_dto.getPrimary_prefix());
            ps.setString(4, p_dto.getOther_prefixes());
            ps.setInt(5, p_dto.getStatus());
            ps.executeUpdate();

            AnsLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error in MVTSPRO.");
            logger.fatal("Error while adding ANS: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remotePS != null) {
                    remotePS.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }

            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleANSDelete(long ansIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update ans_clients set deleted=1 where id in(" + Utils.arrayToString(ansIds) + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception ex) {
            logger.fatal("Error while deleting ANS ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleANSStatusUpdate(long ansIds[], int status) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update ans_clients set status=" + status + " where id in(" + Utils.arrayToString(ansIds) + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception ex) {
            logger.fatal("Error while updating ANS ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
