package ans;

import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class AnsForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    public long id;
    public String name;
    public String company_name;
    public String primary_prefix;
    public String other_prefixes;
    public int status;
    public int deleted;
    public boolean sw_primary_prefix;
    public boolean sw_other_prefix;
    public boolean sw_name;
    public boolean sw_company_name;
    public String message;
    public String activateBtn;
    public String inactiveBtn;
    public String deleteBtn;
    public long[] selectedIDs;
    public ArrayList<AnsDTO> list;

    public AnsForm() {
        super();
        status = 1;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOther_prefixes() {
        return other_prefixes;
    }

    public void setOther_prefixes(String other_prefixes) {
        this.other_prefixes = other_prefixes;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public String getPrimary_prefix() {
        return primary_prefix;
    }

    public void setPrimary_prefix(String primary_prefix) {
        this.primary_prefix = primary_prefix;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    public String getActivateBtn() {
        return activateBtn;
    }

    public void setActivateBtn(String activateBtn) {
        this.activateBtn = activateBtn;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public String getInactiveBtn() {
        return inactiveBtn;
    }

    public void setInactiveBtn(String inactiveBtn) {
        this.inactiveBtn = inactiveBtn;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public ArrayList<AnsDTO> getList() {
        return list;
    }

    public void setList(ArrayList<AnsDTO> list) {
        this.list = list;
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getPrimary_prefix() == null || getPrimary_prefix().length() < 1) {
            errors.add("primary_prefix", new ActionMessage("errors.required", "Primary Prefix"));
        } else if (getPrimary_prefix() != null && getPrimary_prefix().length() != 3) {
            errors.add("primary_prefix", new ActionMessage("errors.max_digit_length", "Primary Prefix", "3"));
        } else if (request.getParameter("primary_prefix") != null && !Utils.isInteger(request.getParameter("primary_prefix"))) {
            errors.add("primary_prefix", new ActionMessage("errors.integer", "Primary Prefix"));
        }
        if (getName() == null || getName().length() < 1) {
            errors.add("name", new ActionMessage("errors.required", "Name"));
        }
        if (getCompany_name() == null || getCompany_name().length() < 1) {
            errors.add("company_name", new ActionMessage("errors.required", "Company Name"));
        }
        return errors;
    }
}
