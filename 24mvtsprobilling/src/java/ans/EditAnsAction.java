package ans;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;

public class EditAnsAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        AnsForm formBean = (AnsForm) form;
        if (login_dto != null && login_dto.getSuperUser()) {
            AnsDTO dto = new AnsDTO();
            AnsTaskSchedular scheduler = new AnsTaskSchedular();
            dto.setId(formBean.getId());
            dto.setName(formBean.getName());
            dto.setPrimary_prefix(formBean.getPrimary_prefix());
            dto.setOther_prefixes(formBean.getOther_prefixes());
            dto.setCompany_name(formBean.getCompany_name());
            dto.setStatus(formBean.getStatus());
            MyAppError error = scheduler.editAnsInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {

                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getAnsDTO(formBean.getId())));
                ac_dto.setActionName(Constants.EDIT_ACTION);
                ac_dto.setTableName("ans_clients");
                ac_dto.setPrimaryKey(dto.getPrimary_prefix());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage(false, "ANS is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
