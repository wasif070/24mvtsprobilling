package aggregatedreports;

import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class AggregateReportTaskSchedular {

    public AggregateReportTaskSchedular() {
    }

    public ArrayList<AggregateReportDTO> getDTOs(AggregateReportDTO p_dto) {
        AggregateReportLoader reportLoader = new AggregateReportLoader();
        return reportLoader.getDTOs(p_dto);
    }

    public MyAppError deleteReports(AggregateReportDTO p_dto) {
        AggregateReportLoader reportLoader = new AggregateReportLoader();
        return reportLoader.deleteReport(p_dto);
    }
}
