package aggregatedreports;

public class AggregateReportDTO {

    public long origin_client_id;
    public long term_client_id;
    public String disconnect_time;
    public String disconnect_time_str;
    public String total_duration;
    private String total_duration_sec;
    public String client_id_str;
    public boolean swClient = false;
    public boolean swTermClient = false;
    public boolean swDateTime = false;
    private String from_date;
    private String to_date;
    private int callType;

    public String getDisconnect_time() {
        return disconnect_time;
    }

    public void setDisconnect_time(String disconnect_time) {
        this.disconnect_time = disconnect_time;
    }

    public String getDisconnect_time_str() {
        return disconnect_time_str;
    }

    public void setDisconnect_time_str(String disconnect_time_str) {
        this.disconnect_time_str = disconnect_time_str;
    }

    public long getOrigin_client_id() {
        return origin_client_id;
    }

    public void setOrigin_client_id(long origin_client_id) {
        this.origin_client_id = origin_client_id;
    }

    public String getTotal_duration() {
        return total_duration;
    }

    public void setTotal_duration(String total_duration) {
        this.total_duration = total_duration;
    }

    public String getClient_id_str() {
        return client_id_str;
    }

    public void setClient_id_str(String client_id_str) {
        this.client_id_str = client_id_str;
    }

    public String getTotal_duration_sec() {
        return total_duration_sec;
    }

    public void setTotal_duration_sec(String total_duration_sec) {
        this.total_duration_sec = total_duration_sec;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public long getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(long term_client_id) {
        this.term_client_id = term_client_id;
    }
    
}
