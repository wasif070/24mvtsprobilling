/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class EditOperatorsAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        OperatorsForm formBean = (OperatorsForm) form;
        if (login_dto != null && login_dto.getSuperUser()) {
            OperatorDTO dto = new OperatorDTO();
            OperatorTaskScheduler scheduler = new OperatorTaskScheduler();
            dto.setOp_id(formBean.getOperator_id());
            dto.setOp_name(formBean.getOperator_name());
            dto.setOp_prefix(formBean.getOperator_prefix());

            MyAppError error = scheduler.editOperatorInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getOperatorDTO((int)(long)dto.getOp_id())));
                ac_dto.setActionName(Constants.EDIT_ACTION);
                ac_dto.setTableName("operators");
                ac_dto.setPrimaryKey(dto.getOp_prefix());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage("Operator is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        
        
        return mapping.findForward(target);
    }
    
}
