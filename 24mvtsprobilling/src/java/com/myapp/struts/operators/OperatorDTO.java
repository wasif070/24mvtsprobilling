/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

/**
 *
 * @author reefat
 */
public class OperatorDTO {
    private Long op_id;
    private String op_prefix;
    private String op_name;
    public boolean searchWithPrefix = false;
    private int is_deleted;

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Long getOp_id() {
        return op_id;
    }

    public void setOp_id(Long op_id) {
        this.op_id = op_id;
    }

    public String getOp_name() {
        return op_name;
    }

    public void setOp_name(String op_name) {
        this.op_name = op_name;
    }

    public String getOp_prefix() {
        return op_prefix;
    }

    public void setOp_prefix(String op_prefix) {
        this.op_prefix = op_prefix;
    }
    
}
