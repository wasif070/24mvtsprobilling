/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class ListOperatorsAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            OperatorTaskScheduler scheduler = new OperatorTaskScheduler();
            OperatorsForm opForm = (OperatorsForm) form;

            if (list_all == 0) {
                if (opForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, opForm.getRecordPerPage());
                }
                OperatorDTO ddto = new OperatorDTO();
                if (opForm.getOperator_prefix() != null && opForm.getOperator_prefix().trim().length() > 0) {
                    ddto.searchWithPrefix = true;
                    ddto.setOp_prefix(opForm.getOperator_prefix().toLowerCase());
                }
                opForm.setOperatorList(scheduler.getOperatorDTOsWithSearchParam(ddto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                    opForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString()));
                }
                opForm.setOperatorList(scheduler.getOperatorDTOs(login_dto));
            }

            if (opForm.getOperatorList() != null && opForm.getOperatorList().size() <= (opForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                //logger.debug("AAAA" + opForm.getOperatorList().size());
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
    }
    
}
