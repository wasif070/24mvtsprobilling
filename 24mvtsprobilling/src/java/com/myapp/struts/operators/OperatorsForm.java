/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author reefat
 */
public class OperatorsForm extends org.apache.struts.action.ActionForm {
    
    private String name;
    private int number;
    private int doValidate;    
    private Long operator_id;    
    private String message;
    private String operator_name;
    private String operator_prefix;
    private int pageNo;
    private int recordPerPage;
    private ArrayList<OperatorDTO> operatorList;  
    private long[] selectedIDs;
    private String deleteBtn;

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }
    
    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }
    
    

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }
    
    

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public ArrayList<OperatorDTO> getOperatorList() {
        return operatorList;
    }

    public void setOperatorList(ArrayList<OperatorDTO> operatorList) {
        this.operatorList = operatorList;
    }

    public Long getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(Long operator_id) {
        this.operator_id = operator_id;
    }
    
    public String getOperator_name() {
        return operator_name;
    }

    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }

    public String getOperator_prefix() {
        return operator_prefix;
    }

    public void setOperator_prefix(String operator_prefix) {
        this.operator_prefix = operator_prefix;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @return
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param i
     */
    public void setNumber(int i) {
        number = i;
    }

    /**
     *
     */
    public OperatorsForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getOperator_prefix() == null || getOperator_prefix().length() < 1) {
                errors.add("operator_prefix", new ActionMessage("errors.operator_prefix.required"));
                // TODO: add 'error.name.required' key to your resources
            }        
        }
        return errors;
    }
}
