/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.util;

/**
 *
 * @author Administrator
 */
public class AppError {

    public static int NO_ERROR = 0;
    public static int VALIDATION_ERROR = 1;
    public static int DB_ERROR = 3;
    public static int OTHERS_ERROR = 3;
    public int errorType;
    public String errorMessage;

    public AppError() {
        errorType = NO_ERROR;
        errorMessage = "No error detected.";
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }
}
