/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.gateway;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class DeleteGatewayAction extends Action {

    static Logger logger = Logger.getLogger(GatewayDAO.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        long cid = Long.parseLong(request.getParameter("id"));
        GatewayTaskSchedular rt = new GatewayTaskSchedular();
        GatewayDTO dto=rt.getGatewayDTO(cid);

        MyAppError error = new MyAppError();
        error = rt.deleteGateway((int)cid);
        
        dto.setIs_deleted(1);
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        Gson json = new GsonBuilder().serializeNulls().create();
        ActivityDTO ac_dto = new ActivityDTO();
        ac_dto.setUserId(login_dto.getClientId());
        ac_dto.setChangedValue(json.toJson(rt.getGatewayDTO(cid)));
        ac_dto.setActionName(Constants.DELETE_ACTION);
        ac_dto.setTableName("gateway");
        ac_dto.setPrimaryKey(dto.getGateway_name());
        ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
        activityTaskScheduler.addActivityDTO(ac_dto);
        
        GatewayForm formBean = new GatewayForm();
        formBean.setMessage(error.getErrorType() > 0 ? true : false, error.getErrorMessage());
        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
        return mapping.findForward("success");
    }
}
