/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class GetClientCapacityGroupAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String org_ip = request.getParameter("org");
        String term_ip = request.getParameter("term");
        String gw_ip = "";
        if(org_ip != null)
            gw_ip = org_ip;
        else if(term_ip != null)
            gw_ip = term_ip;
        if (login_dto != null && login_dto.getSuperUser()) {
            CapacityGroupForm formBean = (CapacityGroupForm) form;
            CapacityGroupDTO dto = new CapacityGroupDTO();
            CapacityGroupTaskScheduler scheduler = new CapacityGroupTaskScheduler();
            dto = scheduler.getClientCapacityGrpDTOByID(login_dto,gw_ip);
            if (dto != null) {
                formBean.setClientId(dto.getCg_client_id());                
                formBean.setGateway_type_name(dto.getCg_gateway_type());
                formBean.setGw_ip(gw_ip);
                if(dto.getCg_org_capacity_group()!=null)
                    formBean.setCapacitygrpName(dto.getCg_org_capacity_group());
                else if(dto.getCg_term_capacity_group()!=null)
                    formBean.setCapacitygrpName(dto.getCg_term_capacity_group());                
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
    }
    
}
