/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class EditCapacityGroupAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        MyAppError error = new MyAppError();

        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        CapacityGroupForm formBean = (CapacityGroupForm) form;
        if (login_dto != null && login_dto.getSuperUser()) {
            CapacityGroupDTO dto = new CapacityGroupDTO();
            CapacityGroupTaskScheduler scheduler = new CapacityGroupTaskScheduler();
            
            dto.setGroup_id(Integer.parseInt(formBean.getCapacitygrpId()));
            dto.setGroup_name(formBean.getCapacitygrpName());
            dto.setDescription(formBean.getCapacitygrpDesc());
            dto.setParent_group_id(formBean.getCapacitygrpParentGrp());
            dto.setCapacity(formBean.getCapacitygrpCapacity());
            
            int check_or_not = 0;
            if(dto.getGroup_id()!=Integer.parseInt(dto.getParent_group_id()))
                check_or_not = scheduler.checkCapacityGrpParentLimit(dto);
            else
                check_or_not = 3;
            
            if(check_or_not == 0){
                error = scheduler.editCapacityGroup(dto);

                if (error.getErrorType() > 0) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else {
                    Gson json = new GsonBuilder().serializeNulls().create();
                    ActivityDTO ac_dto = new ActivityDTO();
                    ac_dto.setUserId(login_dto.getClientId());
                    ac_dto.setChangedValue(json.toJson(scheduler.getCapacityGroupDTO(dto.getGroup_id())));
                    ac_dto.setActionName(Constants.EDIT_ACTION);
                    ac_dto.setTableName("mvts_capacity_group");
                    ac_dto.setPrimaryKey(String.valueOf(dto.getGroup_id()));
                    ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                    activityTaskScheduler.addActivityDTO(ac_dto);

                    formBean.setMessage(false, "Capacity Group is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                    return changedActionForward;
                }            
            } else{
                if(check_or_not == 1)
                    formBean.setMessage(true, "Child capacity exceeds parent capacity");
                else if(check_or_not == 2)
                    formBean.setMessage(true, "Capacity Group Name Exists");
                else if(check_or_not == 3)
                    formBean.setMessage(true, "Capacity Group Can't refer itself as parent");                
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
    }
    
}
