/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class GetCapacityGroupAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        int id = Integer.parseInt(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            CapacityGroupForm formBean = (CapacityGroupForm) form;
            CapacityGroupDTO dto = new CapacityGroupDTO();
            CapacityGroupTaskScheduler scheduler = new CapacityGroupTaskScheduler();
            dto = scheduler.getCapacityGroupDTO(id);
            if (dto != null) {
                formBean.setCapacitygrpId(String.valueOf(dto.getGroup_id()));
                formBean.setCapacitygrpName(dto.getGroup_name());
                formBean.setCapacitygrpDesc(dto.getDescription());
                formBean.setCapacitygrpCapacity(dto.getCapacity());
                formBean.setCapacitygrpParentGrp(dto.getParent_group_id());
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
                request.getSession(true).setAttribute("id", dto.getGroup_id());
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
    }
    
}
