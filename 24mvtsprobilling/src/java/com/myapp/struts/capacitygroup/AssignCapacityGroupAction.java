/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class AssignCapacityGroupAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        Logger logger = Logger.getLogger(AssignCapacityGroupAction.class.getName());
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            CapacityGroupForm formBean = (CapacityGroupForm) form;
            CapacityGroupDTO dto = new CapacityGroupDTO();
            CapacityGroupTaskScheduler scheduler = new CapacityGroupTaskScheduler();
            
            dto.setMappedClientId(formBean.getClient_id());
            dto.setGroup_id(Integer.parseInt(formBean.getCapacitygrp()));
            dto.setGatewayType(formBean.getGateway_type());
            MyAppError error = scheduler.mapCapacityGroup(login_dto,dto) ;
            
            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                //ac_dto.setChangedValue(json.toJson(scheduler.getCapacityGroupDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.UPDATE_ACTION);
                ac_dto.setTableName("mvts_gateway");
                ac_dto.setPrimaryKey(String.valueOf(dto.getGroup_id()));
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
                
                formBean.setMessage(false, "Capacity Group is assigned successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());                
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }            
            
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return mapping.findForward(target);
    }
    
}
