/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.session.Constants;

/**
 *
 * @author reefat
 */
public class CarrierColumnMappingAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        request.getSession(true).setAttribute(Constants.RESULT_MESSAGE,null);
        DisputeForm disputeform = (DisputeForm) form;
        int result_msg = 0;
        if(request.getParameter("result_msg") !=null){
            result_msg = Integer.parseInt(request.getParameter("result_msg"));
            request.getSession(true).setAttribute(Constants.RESULT_MESSAGE, null);
        }
        else{
            if(disputeform.getN_o_c()=="")                
            Constants.NUMBER_OF_COLUMNS = 1;
            else
            Constants.NUMBER_OF_COLUMNS = Integer.parseInt(disputeform.getN_o_c());
        }
                

        return (mapping.findForward(target)); 
    }
    
}