/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.Utils;
import com.myapp.struts.session.Constants;

/**
 *
 * @author reefat
 */
public class DateWiseSummaryAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      
        String target = "success";  
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);        
        if (login_dto != null && login_dto.getSuperUser()){
            int pageNo = 1;
            int list_all = 0; 
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            DisputeTaskScheduler scheduler = new DisputeTaskScheduler();
            DisputeForm disputeForm = (DisputeForm) form;

            
            if (disputeForm.getRecordPerPage() > 0) {
                request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, disputeForm.getRecordPerPage());
            }            
            
        if (request.getParameter("list_all") != null) {
            list_all = Integer.parseInt(request.getParameter("list_all"));
        }
        
        
        if(list_all==0){
            String startTime = disputeForm.getFromYear_dateWise() + "-" + disputeForm.getFromMonth_dateWise() + "-" + disputeForm.getFromDay_dateWise() + " " + "0:0:0";
            String endDate = disputeForm.getToYear_dateWise() + "-" + disputeForm.getToMonth_dateWise() + "-" + disputeForm.getToDay_dateWise() + " " + "23:59:59";
            int carrier_id = disputeForm.getCarrier_id_datewise_summary();
//            String radio_choice = disputeForm.getDateWiseSummaryRadio();
            disputeForm.setDisputeDTOs_datewise_summary(scheduler.getDisputeDTOsWithSearchParameterDate("DisputeDatewiseSummary",carrier_id,startTime,endDate));
        }
        else
        {
            disputeForm.setDisputeDTOs_datewise_summary(null);
        }        

        if(disputeForm.getDisputeDTOs_datewise_summary()!=null && disputeForm.getDisputeDTOs_datewise_summary().size() <= (disputeForm.getRecordPerPage() * (pageNo - 1))){
            ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
            return changedActionForward;
        }
            
        }
        else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
    
}