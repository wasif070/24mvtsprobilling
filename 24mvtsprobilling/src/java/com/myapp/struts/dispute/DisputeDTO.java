/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

/**
 *
 * @author reefat
 */
public class DisputeDTO {
    
    private long no_of_cdr_summary;
    private String total_duration_summary;
    private String cdr_owner;    
    
//    private String hour_datewise_summary;
    private String date_datewise_summary;
    private String date_datewise_summary_carrier;    
    private String total_calls_ipvision_datewise_summary;
    private String total_duration_ipvision_datewise_summary;
    private String total_calls_carrier_datewise_summary;
    private String total_duration_carrier_datewise_summary;    
    private String deviation_datewise_summary;
    
    private String cdr_date_details;
    private String caller_number_details;
    private String called_number_details;
    private String duration_ipvision_details;
    private String duration_carrier_details;
    
    private String ID;
    private String ClientID;

//    public String getHour_datewise_summary() {
//        return hour_datewise_summary;
//    }
//
//    public void setHour_datewise_summary(String hour_datewise_summary) {
//        this.hour_datewise_summary = hour_datewise_summary;
//    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String ClientID) {
        this.ClientID = ClientID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCalled_number_details() {
        return called_number_details;
    }

    public void setCalled_number_details(String called_number_details) {
        this.called_number_details = called_number_details;
    }

    public String getCaller_number_details() {
        return caller_number_details;
    }

    public void setCaller_number_details(String caller_number_details) {
        this.caller_number_details = caller_number_details;
    }

    public String getCdr_date_details() {
        return cdr_date_details;
    }

    public void setCdr_date_details(String cdr_date_details) {
        this.cdr_date_details = cdr_date_details;
    }

    public String getDuration_carrier_details() {
        return duration_carrier_details;
    }

    public void setDuration_carrier_details(String duration_carrier_details) {
        this.duration_carrier_details = duration_carrier_details;
    }

    public String getDuration_ipvision_details() {
        return duration_ipvision_details;
    }

    public void setDuration_ipvision_details(String duration_ipvision_details) {
        this.duration_ipvision_details = duration_ipvision_details;
    }

    public String getDate_datewise_summary_carrier() {
        return date_datewise_summary_carrier;
    }

    public void setDate_datewise_summary_carrier(String date_datewise_summary_carrier) {
        this.date_datewise_summary_carrier = date_datewise_summary_carrier;
    }

    public String getDate_datewise_summary() {
        return date_datewise_summary;
    }

    public void setDate_datewise_summary(String date_datewise_summary) {
        this.date_datewise_summary = date_datewise_summary;
    }

    public String getDeviation_datewise_summary() {
        return deviation_datewise_summary;
    }

    public void setDeviation_datewise_summary(String deviation_datewise_summary) {
        this.deviation_datewise_summary = deviation_datewise_summary;
    }

    public String getTotal_calls_ipvision_datewise_summary() {
        return total_calls_ipvision_datewise_summary;
    }

    public void setTotal_calls_ipvision_datewise_summary(String total_calls_datewise_summary) {
        this.total_calls_ipvision_datewise_summary = total_calls_datewise_summary;
    }

    public String getTotal_calls_carrier_datewise_summary() {
        return total_calls_carrier_datewise_summary;
    }

    public void setTotal_calls_carrier_datewise_summary(String total_calls_carrier_datewise_summary) {
        this.total_calls_carrier_datewise_summary = total_calls_carrier_datewise_summary;
    }

    public String getTotal_duration_ipvision_datewise_summary() {
        return total_duration_ipvision_datewise_summary;
    }

    public void setTotal_duration_ipvision_datewise_summary(String total_duration_datewise_summary) {
        this.total_duration_ipvision_datewise_summary = total_duration_datewise_summary;
    }

    public String getTotal_duration_carrier_datewise_summary() {
        return total_duration_carrier_datewise_summary;
    }

    public void setTotal_duration_carrier_datewise_summary(String total_duration_carrier_datewise_summary) {
        this.total_duration_carrier_datewise_summary = total_duration_carrier_datewise_summary;
    }
    

    public String getCdr_owner() {
        return cdr_owner;
    }

    public void setCdr_owner(String cdr_owner) {
        this.cdr_owner = cdr_owner;
    }

    public long getNo_of_cdr_summary() {
        return no_of_cdr_summary;
    }

    public void setNo_of_cdr_summary(long no_of_cdr_of_carrier_summary) {
        this.no_of_cdr_summary = no_of_cdr_of_carrier_summary;
    }

    public String getTotal_duration_summary() {
        return total_duration_summary;
    }

    public void setTotal_duration_summary(String total_duration_of_carrier_summary) {
        this.total_duration_summary = total_duration_of_carrier_summary;
    }

}