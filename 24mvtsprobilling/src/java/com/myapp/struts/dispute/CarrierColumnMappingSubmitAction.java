/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;
import com.myapp.struts.session.Constants;
import org.apache.struts.upload.FormFile;
/**
 *
 * @author reefat
 */
public class CarrierColumnMappingSubmitAction extends Action {
    static Logger logger = Logger.getLogger(CarrierColumnMappingSubmitAction.class.getName());
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String target = "success";
        int result_msg = 0;
        String result = "";
        if (request.getParameter("result_msg") != null) {
            result_msg = Integer.parseInt(request.getParameter("list_all"));
        }
        
        DisputeForm disputeform = (DisputeForm) form;
        
        FormFile myFile = disputeform.getTheFile();
        String fileName = myFile.getFileName();
        if ((fileName == null) || fileName.indexOf("csv") == -1) {
            request.getSession(true).setAttribute(Constants.MESSAGE, "Invalid File Type!");
            return mapping.findForward("failure");
        } else {
            DisputeTaskScheduler dispute_scheduler = new DisputeTaskScheduler();
            String[] xx = disputeform.getOur_db_columns();        
            result = dispute_scheduler.insertCarrierDataForDispute(myFile,xx);        
        }
        

        
        if(result_msg!=0){
            request.getSession(true).setAttribute(Constants.RESULT_MESSAGE,null);
        }
        else            
        request.getSession(true).setAttribute(Constants.RESULT_MESSAGE, result);        
        
        return (mapping.findForward(target)); 
    }
    
}