/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

/**
 *
 * @author Ashraful
 */
public class CDRQualityDTO {

    private long period_start;
    private long period_end;
    private long origin_client_id;
    private String origin_client_name;
    private String origin_ip;
    private String origin_destination;
    private double origin_bill_amount;
    private long term_client_id;
    private String term_client_name;
    private String term_ip;
    private String term_destination;
    private double term_bill_amount;
    private long duration;
    private long asr;
    private double acd;
    private double avg_pdd;
    private int total_success;
    private int total_fail;
    private String start_time;
    private String origin_caller;
    private String terminated_no;
    private int total_client;

    public CDRQualityDTO() {
    }

    public double getAcd() {
        return acd;
    }

    public void setAcd(double acd) {
        this.acd = acd;
    }

    public long getAsr() {
        return asr;
    }

    public void setAsr(long asr) {
        this.asr = asr;
    }

    public double getAvg_pdd() {
        return avg_pdd;
    }

    public void setAvg_pdd(double avg_pdd) {
        this.avg_pdd = avg_pdd;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getOrigin_client_id() {
        return origin_client_id;
    }

    public void setOrigin_client_id(long origin_client_id) {
        this.origin_client_id = origin_client_id;
    }

    public String getOrigin_client_name() {
        return origin_client_name;
    }

    public void setOrigin_client_name(String origin_client_name) {
        this.origin_client_name = origin_client_name;
    }

    public String getOrigin_destination() {
        return origin_destination;
    }

    public void setOrigin_destination(String origin_destination) {
        this.origin_destination = origin_destination;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public double getOrigin_bill_amount() {
        return origin_bill_amount;
    }

    public void setOrigin_bill_amount(double origin_bill_amount) {
        this.origin_bill_amount = origin_bill_amount;
    }

    public long getPeriod_end() {
        return period_end;
    }

    public void setPeriod_end(long period_end) {
        this.period_end = period_end;
    }

    public long getPeriod_start() {
        return period_start;
    }

    public void setPeriod_start(long period_start) {
        this.period_start = period_start;
    }

    public long getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(long term_client_id) {
        this.term_client_id = term_client_id;
    }

    public String getTerm_client_name() {
        return term_client_name;
    }

    public void setTerm_client_name(String term_client_name) {
        this.term_client_name = term_client_name;
    }

    public String getTerm_destination() {
        return term_destination;
    }

    public void setTerm_destination(String term_destination) {
        this.term_destination = term_destination;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public double getTerm_bill_amount() {
        return term_bill_amount;
    }

    public void setTerm_bill_amount(double term_bill_amount) {
        this.term_bill_amount = term_bill_amount;
    }

    public int getTotal_fail() {
        return total_fail;
    }

    public void setTotal_fail(int total_fail) {
        this.total_fail = total_fail;
    }

    public int getTotal_success() {
        return total_success;
    }

    public void setTotal_success(int total_success) {
        this.total_success = total_success;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public String getTerminated_no() {
        return terminated_no;
    }

    public void setTerminated_no(String terminated_no) {
        this.terminated_no = terminated_no;
    }

    public int getTotal_client() {
        return total_client;
    }

    public void setTotal_client(int total_client) {
        this.total_client = total_client;
    }
}
