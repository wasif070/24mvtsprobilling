package com.myapp.struts.reports;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Wasif
 */
public class ListErrorReportAction extends Action {

    static Logger logger = Logger.getLogger(ListCDRReportAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {

        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            request.getSession(true).setAttribute(Constants.MESSAGE, null);
            ReportTaskScheduler scheduler = new ReportTaskScheduler();
            CDRReportForm cdrReportForm = (CDRReportForm) form;
            CDRReportDTO sdto = new CDRReportDTO();
            NumberFormat formatter = new DecimalFormat("00");
            if (cdrReportForm.getDoSearch() != null) {
                sdto.setErrorType(cdrReportForm.getErrorType());
                String fromDate = cdrReportForm.getFromYear() + "-" + formatter.format(cdrReportForm.getFromMonth()) + "-" + formatter.format(cdrReportForm.getFromDay()) + " " + formatter.format(cdrReportForm.getFromHour()) + ":" + formatter.format(cdrReportForm.getFromMin()) + ":00";
                sdto.setFromDate(fromDate);
                String toDate = cdrReportForm.getToYear() + "-" + formatter.format(cdrReportForm.getToMonth()) + "-" + formatter.format(cdrReportForm.getToDay()) + " " + formatter.format(cdrReportForm.getToHour()) + ":" + formatter.format(cdrReportForm.getToMin()) + ":00";
                sdto.setToDate(toDate);
                sdto.setErrorType(cdrReportForm.getErrorType());
                sdto.setOrigin_ip(cdrReportForm.getOrigin_ip());
                sdto.setTerminated_no(cdrReportForm.getTerminated_no());
            } else {
                sdto.setErrorType(1);
                cdrReportForm.setErrorType(1);
            }
            cdrReportForm.setErrorList(scheduler.getErrorReportDTOs(sdto, login_dto));
            request.getSession(true).setAttribute("SearchErrorDTO", sdto);

            if (cdrReportForm.getErrorList() != null && cdrReportForm.getErrorList().size() > 0) {
                request.getSession(true).setAttribute("ErrorReportDTO", cdrReportForm.getErrorList());
            } else {
                request.getSession(true).setAttribute("ErrorReportDTO", null);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "error";
        }
        return (mapping.findForward(target));
    }
}
