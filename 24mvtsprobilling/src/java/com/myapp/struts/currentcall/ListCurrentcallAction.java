package com.myapp.struts.currentcall;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class ListCurrentcallAction extends Action {

    static Logger logger = Logger.getLogger(ListCurrentcallAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }
            CurrentcallTaskSchedular scheduler = new CurrentcallTaskSchedular();
            CurrentcallForm currentcallForm = (CurrentcallForm) form;
            if (list_all == 0) {
                if (currentcallForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, currentcallForm.getRecordPerPage());
                }
                CurrentcallDTO cdto = new CurrentcallDTO();
                if (currentcallForm.getOrigin_client_name() != null && currentcallForm.getOrigin_client_name().trim().length() > 0) {
                    cdto.searchWithOrgGatewayName = true;
                    cdto.setOrigin_client_name(currentcallForm.getOrigin_client_name().trim().toLowerCase());
                }
                if (currentcallForm.getCurrent_dial_no_in() != null && currentcallForm.getCurrent_dial_no_in().trim().length() > 0) {
                    cdto.searchWithOrgPrefix = true;
                    cdto.setCurrent_dial_no_in(currentcallForm.getCurrent_dial_no_in().trim().toLowerCase());
                }
                if (currentcallForm.getTerm_client_name() != null && currentcallForm.getTerm_client_name().trim().length() > 0) {
                    cdto.searchWithTermGatewayName = true;
                    cdto.setTerm_client_name(currentcallForm.getTerm_client_name().trim().toLowerCase());
                }
                if (currentcallForm.getCurrent_dial_no_out() != null && currentcallForm.getCurrent_dial_no_out().trim().length() > 0) {
                    cdto.searchWithTermPrefix = true;
                    cdto.setCurrent_dial_no_out(currentcallForm.getCurrent_dial_no_out().trim().toLowerCase());
                }

                if (currentcallForm.getStatus() > -1) {
                    cdto.swStatus = true;
                    cdto.setStatus(currentcallForm.getStatus());
                }
                currentcallForm.setCurrentcallList(scheduler.getCurrentcallDTOsWithSearchParam(cdto,login_dto.getOwn_id()));
                target = "success";
            } else {
                currentcallForm.setCurrentcallList(scheduler.getCurrentcallDTOs(login_dto.getOwn_id()));
                target = "success";
            }

            ArrayList<CurrentcallDTO> curCalls = currentcallForm.getCurrentcallList();
            int total_calling = 0;
            int total_connected = 0;
            if (curCalls != null && curCalls.size() > 0) {
                for (CurrentcallDTO ccdto : curCalls) {
                    if (ccdto.getStatus() == 0) {
                        total_calling++;
                    } else {
                        total_connected++;
                    }
                }
            }
            request.getSession(true).setAttribute("CALLING_CALLS", total_calling);
            request.getSession(true).setAttribute("CONNECTED_CALLS", total_connected);
            request.getSession(true).setAttribute("TOTAL_CURRENT_CALLS", (total_calling + total_connected));

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "failure";
        }

        return (mapping.findForward(target));
    }
}
