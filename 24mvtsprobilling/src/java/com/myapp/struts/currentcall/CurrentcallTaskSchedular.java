package com.myapp.struts.currentcall;

import java.util.ArrayList;

public class CurrentcallTaskSchedular {

    public CurrentcallTaskSchedular() {
    }

    public ArrayList<CurrentcallDTO> getCurrentcallDTOs(long client_id) {
        return CurrentcallLoader.getInstance().getCurrentcallDTOList(client_id);
    }

    public ArrayList<CurrentcallDTO> getCurrentcallDTOsWithSearchParam(CurrentcallDTO udto,long client_id) {
        return CurrentcallLoader.getInstance().getCurrentcallDTOsWithSearchList(udto,client_id);
    }
}
