/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.clients;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class ClientEditAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        ClientForm formBean = (ClientForm) form;
        if (login_dto != null) {
            ClientDTO dto = new ClientDTO();
            ClientTaskSchedular scheduler = new ClientTaskSchedular();
            dto.setId(login_dto.getId());
            dto.setClient_id(formBean.getClient_id());
            dto.setClient_password(formBean.getClient_password());
            dto.setClient_name(formBean.getClient_name());
            dto.setClient_email(formBean.getClient_email());
            dto.setClient_type(formBean.getClient_type());
            dto.setClient_status(formBean.getClient_status());
            dto.setRateplan_id(formBean.getRateplan_id());
            dto.setPrefix(formBean.getPrefix());

            MyAppError error = scheduler.clientEditInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getClientDTO(formBean.getId())));
                ac_dto.setActionName(Constants.EDIT_ACTION);
                ac_dto.setTableName("clients");
                ac_dto.setPrimaryKey(dto.getClient_id());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
                
                formBean.setMessage(false, "Profile has been updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
