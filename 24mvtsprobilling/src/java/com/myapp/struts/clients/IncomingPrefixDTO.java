/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.clients;

/**
 *
 * @author Administrator
 */
public class IncomingPrefixDTO {

    private int id;
    private String incoming_prefix;
    private String incoming_to;
    private int client_id;

    public IncomingPrefixDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getIncoming_prefix() {
        return incoming_prefix;
    }

    public void setIncoming_prefix(String incoming_prefix) {
        this.incoming_prefix = incoming_prefix;
    }

    public String getIncoming_to() {
        return incoming_to;
    }

    public void setIncoming_to(String incoming_to) {
        this.incoming_to = incoming_to;
    }
}
