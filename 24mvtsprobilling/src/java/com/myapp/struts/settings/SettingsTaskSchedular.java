package com.myapp.struts.settings;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class SettingsTaskSchedular {

    public SettingsTaskSchedular() {
    }

    public ArrayList<SettingsDTO> getSettingsDTOs(LoginDTO l_dto) {
        return SettingsLoader.getInstance().getSettingsDTOList();
    }

    public SettingsDTO getSettingsDTOById(int id) {
        return SettingsLoader.getInstance().getSettingsDTOById(id);
    }

    public MyAppError editSettingsInformation(SettingsDTO p_dto) {
        SettingsDAO userDAO = new SettingsDAO();
        return userDAO.editSettingsInformation(p_dto);
    }
}
