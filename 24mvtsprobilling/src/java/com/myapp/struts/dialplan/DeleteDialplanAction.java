package com.myapp.struts.dialplan;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.MyAppError;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeleteDialplanAction extends Action {

    static Logger logger = Logger.getLogger(DeleteDialplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");
        if (login_dto != null && login_dto.getSuperUser() && Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
            int cid = Integer.parseInt(request.getParameter("id"));
            DialplanTaskSchedular rt = new DialplanTaskSchedular();
            DialplanDTO dto = rt.getDialplanDTO(cid);

            MyAppError error = new MyAppError();
            error = rt.deleteDialplan(cid);
            dto.setIs_deleted(1);
            Gson json = new GsonBuilder().serializeNulls().create();
            ActivityDTO ac_dto = new ActivityDTO();
            ac_dto.setUserId(login_dto.getClientId());
            ac_dto.setChangedValue(json.toJson(rt.getDialplanDTO(cid)));
            ac_dto.setActionName(Constants.DELETE_ACTION);
            ac_dto.setTableName("dialpeer");
            ac_dto.setPrimaryKey(dto.getDialplan_name());
            ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
            activityTaskScheduler.addActivityDTO(ac_dto);
            request.getSession(true).setAttribute(Constants.MESSAGE, "Dialplan Deleted Successfully!");
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return mapping.findForward(target);
    }
}