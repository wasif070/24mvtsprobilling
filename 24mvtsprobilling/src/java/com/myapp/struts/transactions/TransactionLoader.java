/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.user.UserDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;
import java.text.DecimalFormat;

/**
 *
 * @author Ashraful
 */
public class TransactionLoader {
    
    static Logger logger = Logger.getLogger(TransactionLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private ArrayList<TransactionDTO> transactionList = null;
    private HashMap<Long, TransactionDTO> summeryList = null;
    private HashMap<Long, TransactionDTO> transactionDTOByID = null;
    static TransactionLoader transactionLoader = null;
    DecimalFormat df = new DecimalFormat("#00.00000");
    
    public TransactionLoader() {
        forceReload();
    }
    
    public static TransactionLoader getInstance() {
        if (transactionLoader == null) {
            createClientLoader();
        }
        return transactionLoader;
    }
    
    private synchronized static void createClientLoader() {
        if (transactionLoader == null) {
            transactionLoader = new TransactionLoader();
        }
    }
    
    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            transactionList = new ArrayList<TransactionDTO>();
            transactionDTOByID = new HashMap<Long, TransactionDTO>();
            summeryList = new HashMap<Long, TransactionDTO>();
            String sql = "select * from client_transactions order by transaction_id DESC";
            ResultSet resultSet = statement.executeQuery(sql);
            TransactionDTO dto = new TransactionDTO();
            while (resultSet.next()) {
                dto = new TransactionDTO();
                dto.setTransaction_id(resultSet.getLong("transaction_id"));
                dto.setClient_id(resultSet.getLong("client_id"));
                if (dto.getClient_id() > 0) {
                    ClientDTO cdto = ClientLoader.getInstance().getClientDTOByID(dto.getClient_id());
                    if (cdto != null) {
                        dto.setClient_name(cdto.getClient_id());
                        dto.setClient_full_name(cdto.getClient_name());
                    }
                }
                dto.setTransaction_recharge(df.format(resultSet.getDouble("transaction_recharge")));
                dto.setTransaction_return(df.format(resultSet.getDouble("transaction_return")));
                dto.setTransaction_receive(df.format(resultSet.getDouble("transaction_receive")));
                dto.setTransaction_date(resultSet.getLong("transaction_date"));
                dto.setTransaction_des(resultSet.getString("transaction_des"));
                dto.setUser_id(resultSet.getLong("user_id"));
                dto.setTransaction_by(resultSet.getInt("transaction_by"));
                dto.setCur_balance(resultSet.getFloat("cur_balance"));
                
                if (dto.getUser_id() > 0) {
                    UserDTO udto = UserLoader.getInstance().getUserDTOByID(dto.getUser_id());
                    if (udto != null) {
                        dto.setUser_name(udto.getUserId());
                    }
                }
                dto.setTransaction_date_string(Utils.LongToDate(dto.getTransaction_date()));
                
                TransactionDTO sdto = summeryList.get(dto.getClient_id());
                if (sdto != null) {
                    sdto.setTransaction_recharge(df.format(Double.parseDouble(sdto.getTransaction_recharge()) + Double.parseDouble(dto.getTransaction_recharge())));
                    sdto.setTransaction_return(df.format(Double.parseDouble(sdto.getTransaction_return()) + Double.parseDouble(dto.getTransaction_return())));
                    sdto.setTransaction_receive(df.format(Double.parseDouble(sdto.getTransaction_receive()) + Double.parseDouble(dto.getTransaction_receive())));
                    sdto.setTransaction_balance(df.format(Double.parseDouble(sdto.getTransaction_balance()) + Double.parseDouble(dto.getTransaction_recharge()) - Double.parseDouble(dto.getTransaction_return()) - Double.parseDouble(dto.getTransaction_receive())));
                } else {
                    sdto = new TransactionDTO();
                    sdto.setClient_id(dto.getClient_id());
                    sdto.setClient_name(dto.getClient_name());
                    sdto.setClient_full_name(dto.getClient_full_name());
                    sdto.setTransaction_recharge(dto.getTransaction_recharge());
                    sdto.setTransaction_return(dto.getTransaction_return());
                    sdto.setTransaction_receive(dto.getTransaction_receive());
                    sdto.setTransaction_balance(df.format(Double.parseDouble(dto.getTransaction_recharge()) - Double.parseDouble(dto.getTransaction_return()) - Double.parseDouble(dto.getTransaction_receive())));
                    sdto.setTransaction_by(dto.getTransaction_by());
                    summeryList.put(dto.getClient_id(), sdto);
                }
                
                transactionDTOByID.put(dto.getTransaction_id(), dto);
                transactionList.add(dto);
            }
            
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in TransactionLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }
    
    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }
    
    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }
    
    public synchronized ArrayList<TransactionDTO> getTransactionDTOList(LoginDTO login_dto, int transaction_type) {
        checkForReload();
        ArrayList<TransactionDTO> newList = new ArrayList<TransactionDTO>();
        for (TransactionDTO dto : transactionList) {
            if (transaction_type == 1) {
                if (dto.getTransaction_by() == login_dto.getOwn_id()) {
                    newList.add(dto);
                }
            } else {
                if (dto.getClient_id() == login_dto.getOwn_id()) {
                    newList.add(dto);
                }
            }
        }
        return newList;
    }
    
    public synchronized ArrayList<TransactionDTO> getTransactionSummeryDTOList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<TransactionDTO> transSummery = new ArrayList<TransactionDTO>();
        ArrayList<TransactionDTO> list = new ArrayList<TransactionDTO>();
        list.addAll(summeryList.values());
        for (TransactionDTO dto : list) {
            if (dto.getTransaction_by() == login_dto.getOwn_id()) {
                transSummery.add(dto);
            }
        }
        return transSummery;
    }
    
    public synchronized TransactionDTO getTransactionDTOByID(long id) {
        checkForReload();
        return transactionDTOByID.get(id);
    }
    
    public synchronized ArrayList<TransactionDTO> getTransactionDTOByClient(long client_id) {
        checkForReload();
        ArrayList<TransactionDTO> transactionListByClient = new ArrayList<TransactionDTO>();
        for (int inc = 0; inc < transactionList.size(); inc++) {
            TransactionDTO dto = transactionList.get(inc);
            if (dto.getClient_id() == client_id) {
                transactionListByClient.add(dto);
            }
        }
        return transactionListByClient;
    }
    
    public synchronized TransactionDTO getTransactionSummeryByClient(long client_id) {
        checkForReload();
        return summeryList.get(client_id);
    }
    
    public ArrayList<TransactionDTO> getTransactionDTOsWithSearchParam(ArrayList<TransactionDTO> list, TransactionDTO tdto, LoginDTO login_dto) {
        ArrayList<TransactionDTO> newList = new ArrayList<TransactionDTO>();;
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                TransactionDTO dto = (TransactionDTO) i.next();
                if (dto.getTransaction_date() < Utils.getDateLong(tdto.getFromDate()) || dto.getTransaction_date() > Utils.getDateLong(tdto.getToDate())) {
                    continue;
                }
                if (tdto.searchWithClientID && tdto.getClient_id() != dto.getClient_id()) {
                    continue;
                }
                newList.add(dto);
            }
        }
        
        return newList;
    }
    
    public ArrayList<TransactionDTO> getTransactionDTOs(ArrayList<TransactionDTO> list, TransactionDTO tdto) {
        ArrayList newList = null;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                TransactionDTO dto = (TransactionDTO) i.next();
                if (dto.getTransaction_date() < Utils.getDateLong(tdto.getFromDate()) || dto.getTransaction_date() > Utils.getDateLong(tdto.getToDate())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }
}
