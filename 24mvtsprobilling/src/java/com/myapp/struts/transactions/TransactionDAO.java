/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.util.Utils;
import java.util.Iterator;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Ashraful
 */
public class TransactionDAO {

    static Logger logger = Logger.getLogger(TransactionDAO.class.getName());

    public TransactionDAO() {
    }

    public ArrayList<TransactionDTO> getTransactionDTOsWithSearchParam(ArrayList<TransactionDTO> list, TransactionDTO tdto) {
        ArrayList newList = null;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();

            while (i.hasNext()) {
                TransactionDTO dto = (TransactionDTO) i.next();
                if (dto.getTransaction_date() < Utils.getDateLong(tdto.getFromDate()) || dto.getTransaction_date() > Utils.getDateLong(tdto.getToDate())) {
                    continue;
                }
                if (tdto.searchWithClientID && tdto.getClient_id() != dto.getClient_id()) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<TransactionDTO> getTransactionDTOs(ArrayList<TransactionDTO> list, TransactionDTO tdto) {
        ArrayList newList = null;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();

            while (i.hasNext()) {
                TransactionDTO dto = (TransactionDTO) i.next();
                if (dto.getTransaction_date() < Utils.getDateLong(tdto.getFromDate()) || dto.getTransaction_date() > Utils.getDateLong(tdto.getToDate())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }
}
