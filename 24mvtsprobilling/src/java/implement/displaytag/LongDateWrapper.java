package implement.displaytag;

import com.myapp.struts.util.Utils;
import javax.servlet.jsp.PageContext;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class LongDateWrapper implements DisplaytagColumnDecorator {

    public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
        if (Long.parseLong(String.valueOf(columnValue)) > 0) {
            return Utils.ToDateDDMMYYhhmm(Long.parseLong(String.valueOf(columnValue)));
        }
        return null;
    }
}
