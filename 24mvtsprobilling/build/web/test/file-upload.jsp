
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("ans/listAns.do?list_all=1;File Upload");
                        navList.add(";Add New File");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/test/fileUpload" method="post" enctype="multipart/form-data">                           
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Add File</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <th valign="top" >File 1 <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="upload" /><br/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >File 2 <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="upload" /><br/>
                                        </td>
                                    </tr>      
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <input name="submit" type="submit" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>           
        </div>
    </body>
</html>
