<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Settings</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "settings");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
            <div class="right_content_view fl_right">
                <div class="pad_10 border_left">
                    <%
                       java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                       navList.add("settings/listSettings.do?list_all=1;Settings");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/settings/listSettings" method="post" >
                        <thead>
                            <tr><th colspan="2" ><h3>Settings</h3></th></tr>
                        </thead>
                        <div class="over_flow_content display_tag_content" align="center">
                            <bean:write name="SettingsForm" property="message" filter="false"/>
                            <div class="jerror_messge"></div>
                            <bean:write name="SettingsForm" property="message" filter="false"/>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.SettingsForm.settingsList" >
                                <display:setProperty name="paging.banner.item_name" value="Gateway" /> 
                                <display:setProperty name="paging.banner.items_name" value="Gateways" />                               
                                <display:column title="Setting Name" class="center-align" sortable="true" style="width:15%" >
                                    ${data.settingName}   
                                </display:column> 
                                <display:column title="Setting Value"  class="center-align" sortable="true" style="width:15%" >                            
                                    ${data.settingsStr}
                                </display:column>            
                                <c:choose>
                                    <c:when test="${editPermission==1}">
                                        <display:column class="center-align" title="Task" style="width:10%;" >
                                            <a href="../settings/getSettings.do?id=${data.id}" class="edit"  title="Change"></a>                                   
                                        </display:column>
                                    </c:when>
                                </c:choose>
                            </display:table>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
