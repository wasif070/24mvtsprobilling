<%@page import="com.myapp.struts.util.AppConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.reports.CDRReportDTO,com.myapp.struts.reports.CDRReportDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>

        <%
            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }
            NumberFormat formatter = new DecimalFormat("00");
            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            ArrayList<CDRReportDTO> errorList = (ArrayList<CDRReportDTO>) request.getSession(true).getAttribute("ErrorReportDTO");
            CDRReportDTO searchDTO = (CDRReportDTO) request.getSession(true).getAttribute("SearchErrorDTO");
            NumberFormat amount_formatter = new DecimalFormat("#00.0000");
            int errorType = 1;
        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Error Report</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "report");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right border_left" style="width:80%">
                <div class="">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";Error Report");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/reports/listError.do" method="post" >
                        <div>
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>From Date</th>
                                    <td class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>To Date</th>
                                    <td class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>   
                                <tr>
                                    <th>Origination IP</th>
                                    <td><html:text property="origin_ip" /></td> 
                                    <th>Terminated No</th>
                                    <td><html:text property="terminated_no" /></td> 
                                </tr>
                                <tr>
                                    <th>Error Type</th>
                                    <td colspan="3"><html:radio property="errorType" value="1" /> Switch Error <html:radio property="errorType" value="2" /> Authentication Error
                                    </td>                                  
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>                  
                </div>
                    <div class="over_flow_content" style="font-size:8pt;"> 
                    <table class="content_table" cellspacing="1" cellpadding="2">
                        <thead>
                            <tr class="header">      
                                <th width="1%">Nr.</th>
                                <% if (searchDTO != null && searchDTO.getErrorType() == AppConstants.SWITCH_ERROR) {%>
                                <th>Switch Error Name</th>
                                <th>Switch Error Count</th>
                                <th>Switch Error Percentage</th>
                                <%} else {%>
                                <th>Auth. Error Name</th>
                                <th>Auth. Error Count</th>
                                <th>Auth. Error Percentage</th>
                                <%}%>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                String bg_class = "odd";
                                if (errorList != null && errorList.size() > 0) {
                                    CDRReportDTO obj = errorList.get(errorList.size() - 1);
                                    double total_fail = obj.getErrorSum();
                                    for (int inc = 0; inc < errorList.size() - 1; inc++) {
                                        obj = errorList.get(inc);
                                        if (inc % 2 == 0) {
                                            bg_class = "even";
                                        } else {
                                            bg_class = "odd";
                                        }

                            %>
                            <tr class="<%=bg_class%> center-align">                                     
                                <td align="right"><%=inc + 1%>.</td>
                                <td><%=obj.getErrorName()%></td>
                                <td><%=obj.getErrorCount()%></td>
                                <td style="text-align: center"><%=amount_formatter.format(obj.getErrorCount() / total_fail * 100)%> %</td>
                            </tr>
                            <%}
                            } else {%>
                            <tr><th colspan="3">No Content Found!</th></tr>
                            <% }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>


