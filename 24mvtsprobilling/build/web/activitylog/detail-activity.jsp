<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="java.util.ArrayList,activitylog.ActivityDTO" %>
        <%@page import="com.myapp.struts.util.AppConstants" %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Client List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "role");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right border_left">               
                <div class="pad_10 ">
                    <%
                        ActivityDTO actDTO = (ActivityDTO) request.getSession(true).getAttribute(AppConstants.ACTIVITY_DTO);
                        request.setAttribute("activityList", actDTO.getComparisonList());
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("activitylog/listActivities.do;Activities");
                        navList.add(";Activity Details");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>                    
                    <div class="display_tag_content" align="center">
                        <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="activityList">
                            <display:column property="fieldName" title="Field Name" sortable="true" headerClass="center-align" class="left-align" style="padding-left:5px" />
                            <display:column property="previousValue" title="Previous Value" sortable="true" headerClass="center-align" class="left-align" style="padding-left:5px"/>                            
                            <display:column property="newValue" title="New Value" sortable="true" headerClass="center-align" class="left-align" style="padding-left:5px"/>                        
                        </display:table>
                        <div class="height-5px"></div> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>  
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>