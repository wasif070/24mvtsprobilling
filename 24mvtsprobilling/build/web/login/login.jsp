<%@page import="com.myapp.struts.settings.SettingsLoader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page language="java"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="com.myapp.struts.session.Constants" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Login</title>
        <div><%@include file="../includes/header.jsp"%></div>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="login_page" align="center">
                <div class="login_table">
                    <html:form action="/login/login" method="post">
                        <h3>Login to  <%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %></h3>
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <th valign="top">User ID</th>
                                    <td valign="top">
                                        <html:text property="loginId"/><br/>
                                        <html:messages id="loginId" property="loginId">
                                            <bean:write name="loginId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">Password</th>
                                    <td valign="top">
                                        <html:password property="loginPass"/><br/>
                                        <html:messages id="loginPass" property="loginPass">
                                            <bean:write name="loginPass"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <input name="submit" type="submit" class="custom-button" value="Login" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h4>
                            <%
                                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                request.getSession(true).removeAttribute(Constants.LOGIN_ACCESS_DENIED);
                                if (request.getSession(true).getAttribute(Constants.LOGIN_TIMEOUT) != null) {
                                    request.getSession(true).removeAttribute(Constants.LOGIN_TIMEOUT);
                                    out.println("<span style='color:red;font-size: 12px;font-weight:bold'>" + Constants.LOGIN_TIMEOUT + "</span>");
                                } else if (request.getSession(true).getAttribute(Constants.LOGIN_ACCESS_DENIED) != null) {
                                    request.getSession(true).removeAttribute(Constants.LOGIN_ACCESS_DENIED);
                                    out.println("<span style='color:red;font-size: 12px;font-weight:bold'>" + Constants.LOGIN_ACCESS_DENIED + "</span>");
                                } else {
                            %>
                            <bean:write name="LoginForm" property="message" filter="false"/>
                            <span style="display:block;" class="blue">A Best Solution for VOIP Billing</span>
                            <%                                                    }
                            %>
                        </h4>
                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                    </html:form>
                </div>
            </div>                     
        </div>
        <div class="clear"></div>
        <div><%@include file="../includes/footer.jsp"%></div>   
    </body>
</html>

<script type="text/javascript">
    $j(document).ready(function(){
        //20 is the footer height
        $j(".main_body").css("height", ($j(document).height()-20));
        $j(".main_body").addClass("clear");
    });
</script>