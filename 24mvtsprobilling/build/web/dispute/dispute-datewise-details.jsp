<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%-- 
    Document   : dispute-datewise-details
    Created on : Sep 2, 2012, 11:27:48 AM
    Author     : reefat
--%>

<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils,com.myapp.struts.clients.ClientDTO" %>


<%
            int pageNo = 1;
            int recordPerPage = 10;

            if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
               boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
                if(status==true){
                  pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
             }   

            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }  
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>24Billing :: Disputed Data</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right border_left">               
                <div class="pad_10 ">
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add("dispute/dispute-summary.jsp;Dispute");                                  
                 navList.add(";Date Wise summary details");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    
                    <html:form action="/dispute/date_wise_details.do" method="post" >
                            <div class="full-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0"  width="100%">                                
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text style="width:50px" property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input style="width:50px" type="text" name="d-49216-p" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="6">
                                            <html:submit styleClass="search-button" value="Show" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </html:form>
                    
                        <div class="display_tag_content" align="center">
                        <%=msg%>
                        <div class="jerror_messge"></div>
                        <script type="text/javascript">count=<%=(pageNo - 1) * recordPerPage%>;</script>
                       
                                <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.DisputeForm.disputeDTOs_datewise_details"  pagesize="<%=recordPerPage%>">
                                    <display:setProperty name="paging.banner.item_name" value="disputedData" /> 
                                    <display:setProperty name="paging.banner.items_name" value="disputedData_s" />                          
                                    <display:column  property="cdr_date_details" class="center-align" title="Date(VisionTel)" sortable="true" style="width:10%" />
                                    <display:column  property="caller_number_details" class="center-align" title="Caller" sortable="true" style="width:10%" />
                                    <display:column property="called_number_details" class="center-align" title="Destination" sortable="true" style="width:10%"></display:column>
                                    <display:column  property="duration_ipvision_details" class="center-align" title="Duration(VisionTel)" sortable="true" style="width:10%" />
                                    <display:column  property="duration_carrier_details" class="center-align" title="Duration(Carrier)" sortable="true" style="width:10%" />
                                </display:table> 
                       
                    </div>
                    
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>