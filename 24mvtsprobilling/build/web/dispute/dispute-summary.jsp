<%-- 
    Document   : dispute-summary
    Created on : Sep 2, 2012, 11:26:46 AM
    Author     : reefat
--%>

<%@page import="com.myapp.struts.dispute.DisputeDTO,com.myapp.struts.dispute.DisputeLoader,java.text.DecimalFormat,java.text.NumberFormat,java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="../login/login-check.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<html>
    <head> 
        <title>24Billing :: Dispute Management</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <%
        ArrayList<Integer> days = Utils.getDay();
        ArrayList<String> months = Utils.getMonth();
        ArrayList<Integer> years = Utils.getYear();
        NumberFormat formatter = new DecimalFormat("00");
    %>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">                   
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add("dispute/dispute-summary.jsp;Dispute");
                 navList.add(";");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>

                    <html:form action="/dispute/disputeSummary.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                <th>From Date</th>
                                <td colspan="3" class="selopt">
                                    <html:select property="fromYear_summary" styleClass="" styleId="fromYear">
                                        <%
                                            for (int i = 0; i < years.size(); i++) {
                                                String year = String.valueOf(years.get(i));
                                        %>
                                        <html:option value="<%=year%>"><%=year%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="fromMonth_summary" styleClass="month" styleId="fromMonth">
                                        <%
                                            for (int i = 0; i < months.size(); i++) {
                                                String month = months.get(i);
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="fromDay_summary" styleClass="">
                                        <%
                                            for (int i = 0; i < days.size(); i++) {
                                                String increment = String.valueOf(i + 1);
                                                String temp = formatter.format((i + 1));
                                        %>
                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                        <%}%>
                                    </html:select>
                                </td>
                                <th>To Date</th>
                                <td colspan="3" class="selopt">
                                    <html:select property="toYear_summary" styleClass="" styleId="fromYear">
                                        <%
                                            for (int i = 0; i < years.size(); i++) {
                                                String year = String.valueOf(years.get(i));
                                        %>
                                        <html:option value="<%=year%>"><%=year%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="toMonth_summary" styleClass="month" styleId="fromMonth">
                                        <%
                                            for (int i = 0; i < months.size(); i++) {
                                                String month = months.get(i);
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="toDay_summary" styleClass="">
                                        <%
                                            for (int i = 0; i < days.size(); i++) {
                                                String increment = String.valueOf(i + 1);
                                                String temp = formatter.format((i + 1));
                                        %>
                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                        <%}%>
                                    </html:select>
                                </td>
                                <th>Carrier</th>
                                <td>
                                    <html:select property="carrier_id_summary" styleId="parentid" >
                                        <html:option value="-1">--Select--</html:option>
                                        <%for (DisputeDTO ds_dto : DisputeLoader.getInstance().getDisputeDTOCarrierList()) {%>
                                        <html:option value="<%=String.valueOf(ds_dto.getID())%>"><%=ds_dto.getClientID()%></html:option>
                                        <%}%>
                                    </html:select>                                        
                                </td>                                 
                                </tr>
                                <tr align="center">
                                <td colspan="30">
                                    <html:submit styleClass="custom-button" value="Start" />
                                    <html:reset styleClass="custom-button" value="Reset" />
                                </td>
                                </tr>                               
                            </table>
                        </div>
                    </html:form>
                    <%
                        ArrayList<DisputeDTO> list = new ArrayList<DisputeDTO>();                        
                        list = (ArrayList<DisputeDTO>) request.getSession(true).getAttribute("showDispute");  
                        if(list!=null){
                        session.removeAttribute("showDispute");
                        for (DisputeDTO dto : list) {
                    %>
                    <div style="margin: 0 auto; display: block">
                        <table cellspacing="0" cellpadding="0" border="0" style="width:50%;" class="reporting_table">
                            <tbody>                                    
                                <tr>                                
                                <td align="right" style="color: #000000; font-weight: bold;padding-right: 5px;border-width: 0;">Total CDR(<%=dto.getCdr_owner() %>):</td>
                                <td align="right" width="35%" style="color: #000000; font-weight: bold;border-width: 0;">
                                    <%=dto.getNo_of_cdr_summary()%>   
                                </td>
                                <td align="right" width="15%" style="color: #0c66ca; font-weight: bold;">&nbsp;</td>
                                </tr>
                                <tr>                                
                                <td align="right" style="color: #000000; font-weight: bold;padding-right: 5px;border-width: 0;">Total Duration(<%=dto.getCdr_owner()%>):</td>
                                <td align="right" width="35%" style="color: #000000; font-weight: bold;border-width: 0;">
                                    <%=dto.getTotal_duration_summary() %>
                                </td>
                                <td align="right" width="15%" style="color: #0c66ca; font-weight: bold;">&nbsp;
                                </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <%}}%>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>              
        </div>
    </body>
</html>