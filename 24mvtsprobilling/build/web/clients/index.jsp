<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.rateplan.RateplanLoader,java.util.ArrayList,com.myapp.struts.util.Utils,com.myapp.struts.clients.ClientDTO" %>


<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 100;

   if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }
   
                long rateplan_id = 0;
                if (login_dto.getOwn_id() > 0) {
                    ClientDTO client_dto = com.myapp.struts.clients.ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
                    if (client_dto != null) {
                        rateplan_id = client_dto.getRateplan_id();
                    }
                }
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Client List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "client");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
            <c:set var="delPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.DELETE])%>"></c:set>
            <c:set var="login_level" value="<%=login_dto.getClient_level() %>"></c:set>
            <div class="right_content_view fl_right border_left">               
                <div class="pad_10 ">
                    <%
                  java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                  navList.add("clients/listClient.do?list_all=1;Clients");
                  navList.add(";");
                  String own_id=request.getParameter("id")!=null? request.getParameter("id"):"-1";
                  String action="/clients/listClient.do?id="+own_id;
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="<%=action%>" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Client ID</th>
                                    <td><html:text property="client_id" /></td>
                                    <th>Client Type</th>
                                    <td>
                                        <html:select property="client_type">
                                            <html:option value="-1">Select Client Type</html:option>
                                            <%
                                                for (int i = 0; i < Constants.CLIENT_TYPE_NAME.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.CLIENT_TYPE[i]%>"><%=Constants.CLIENT_TYPE_NAME[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                    </td>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="client_status">
                                            <html:option value="-1">Select</html:option>
                                            <%
                                             for(int i=0;i<Constants.LIVE_STATUS_VALUE.length;i++)
                                             {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                             }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>" style="width:50px"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=pageNo%>" style="width:50px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10" align="center">                                        
                                        <html:submit styleClass="search-button" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                        <% String parentid=request.getParameter("parent"); %>
                                        <html:hidden property="parent_id" value="<%=parentid%>" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <%
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                            public String addRowClass() {
                                ClientDTO obj = (ClientDTO) getCurrentRowObject();
                                if (Double.parseDouble(obj.getClient_balance()) < ((double)(obj.getClient_call_limit())/obj.getAvg_rate()/60)) {
                                    return "red";
                                } else {
                                    return "";
                                }
                            }
                        });
                    %> 

                    <html:form action="/clients/multipleClient" method="post" onsubmit="return confirm('Are you sure to take action?');" >
                        <div class="over_flow_content display_tag_content" align="center">                            
                            <%=msg%>                            
                            <div class="jerror_messge"></div>                            
                            <c:set var="rateplan_id" value="<%=String.valueOf(rateplan_id)%>"></c:set>    
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" decorator="dyndecorator" cellspacing="0" export="false" id="data" name="sessionScope.ClientForm.clientList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Client" /> 
                                <display:setProperty name="paging.banner.items_name" value="Clients" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='a Client' />" style="width:3%">
                                    <input type="checkbox" name="selectedIDs[]" value="${data.id}" class="select_id"/>
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Client ID" sortProperty="client_id" sortable="true"  style="width:12%;" >
                                    <c:choose>
                                        <c:when test="${login_level<0}">
                                            <c:choose>
                                                <c:when test="${data.client_level==1}">${data.client_id}</c:when>
                                                <c:otherwise><a href="../clients/listClient.do?id=${data.id}&list_all=0" title="Find" style=" text-decoration: underline; color: blue">${data.client_id}</a></c:otherwise>
                                            </c:choose>                                                                                            
                                        </c:when>                                        
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${data.client_level==1}">${data.client_id}</c:when>
                                                <c:otherwise><span style="color: blue">${data.client_id}</span></c:otherwise>
                                            </c:choose>                                                
                                        </c:otherwise>
                                    </c:choose>                                                                                                          
                                </display:column>
                                <display:column title="Incoming Prefix" class="center-align" style="width:10%">
                                    ${data.prefix}
                                </display:column>
                                <display:column property="client_level_str" class="" title="Client Level" sortable="true" style="width:7%; text-align:center"  />
                                <display:column property="client_name" class="" title="Client Name" sortable="true" style="width:13%" />
                                <display:column property="parent_name" class="" title="Parent" sortable="true" style="width:13%" />
                                <display:column property="client_type_name" class="center-align" title="Client Type" sortable="true" style="width:10%" />                                
                                <c:choose>
                                    <c:when test="${data.rateplan_id==rateplan_id}">
                                        <display:column class="" title="Rate Plan" sortable="true" style="width:11%">Base Rateplan</display:column>
                                    </c:when>
                                    <c:when test="${data.rateplan_id<=0}">
                                        <display:column class="red" title="Rate Plan" sortable="true" style="width:11%">No Rateplan</display:column>
                                    </c:when>
                                    <c:otherwise>
                                        <display:column class="" title="Rate Plan" sortable="true" style="width:11%">${data.rateplan_name}</display:column>
                                    </c:otherwise>
                                </c:choose>                                
                                <display:column property="client_call_limit" class="" title="Call Limit" sortable="true" style="width:8%"/>
                                <display:column property="client_balance" class="right-align" title="Balance" sortable="true" style="width:10%;" />                              
                                <display:column property="client_status_name" class="center-align" title="Status" sortable="true" style="width:8%" />
                                <c:choose>
                                    <c:when test="${editPermission==1 || delPermission==1}">
                                        <display:column class="center-align" title="Task" style="width:7%;" >
                                            <c:choose>
                                                <c:when test="${editPermission==1}">
                                                    <a href="../clients/getClient.do?id=${data.id}&parent=${data.parent_id}" class="edit" title="Change"></a>
                                                </c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${delPermission==1}">
                                                    <a href="../clients/deleteClient.do?id=${data.id}" onclick="javascript:return confirm('Are you sure to want delete the Client?');" class="drop" title="Delete"></a>
                                                </c:when>
                                            </c:choose>                                            
                                        </display:column>
                                    </c:when>
                                </c:choose>
                            </display:table>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <div class="button_area">
                                <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                                <%if(perms[com.myapp.struts.util.AppConstants.EDIT]==1){%>
                                <html:submit property="activateBtn" styleClass="custom-button jmultipebtn" value="Activate" />
                                <html:submit property="inactiveBtn" styleClass="custom-button jmultipebtn" value="Inactive" />
                                <html:submit property="blockBtn" styleClass="custom-button jmultipebtn" value="Block" />
                                <%}if(perms[com.myapp.struts.util.AppConstants.DELETE]==1){%>
                                <html:submit property="deleteBtn" styleClass="custom-button jmultipebtn" value="Delete Selected" />
                                <%}%>
                            </div>
                        </div>
                    </html:form>                
                </div>
                <div class="clear"></div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>