<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.util.Utils" %>

        <%           
            int pageNo = 1;
            int sortingOrder = 0;
            int sortedItem = 0;
            int list_all = 0;
            int recordPerPage = 10;

            if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
                 boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
                 if(status==true){
                 pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                  }
               }
            if (request.getParameter("d-49216-s") != null) {
                sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
            }
            if (request.getParameter("d-49216-o") != null) {
                sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
            }
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
           }

            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }

            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            NumberFormat formatter = new DecimalFormat("00");
            
            ArrayList<ClientDTO> clientList = ClientLoader.getInstance().getClientDTOList(login_dto);
        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Client List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "transaction");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right">              
                <div class="pad_10 border_left">
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add(";Payment History");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/transactions/listTransaction.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <th>Client ID</th>
                                    <td>
                                        <html:select property="client_id" styleClass="joriginId">
                                            <html:option value="">Select Client</html:option>
                                            <%
                                                if (clientList != null && clientList.size() > 0) {
                                                    int size = clientList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        ClientDTO c_dto = (ClientDTO) clientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Transaction Type</th>
                                    <td>
                                        <html:select property="transaction_type" styleClass="">                                            
                                            <html:option value="1">Payment Made</html:option>
                                            <html:option value="2">Payment Receive</html:option>
                                        </html:select>                                        
                                    </td>
                                </tr>                                
                                <tr>
                                    <th>From Date</th>
                                    <td class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>To Date</th>
                                    <td class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text style="width:50px" property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input style="width:50px" type="text" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <html:submit styleClass="search-button" value="Search"/>
                                        <html:reset styleClass="search-button" value="Reset" />                                 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="display_tag_content" align="center">
                        <%=msg%>
                        <div class="jerror_messge"></div>
                        <script type="text/javascript">count=<%=(pageNo - 1) * recordPerPage%>;</script>
                        <div class="over_flow_content">
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.TransactionForm.transactionList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Transaction" /> 
                                <display:setProperty name="paging.banner.items_name" value="Transactions" />
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column property="client_name"   title="Client ID" sortProperty="client_id" sortable="true"  style="width:10%" ></display:column>
                                <display:column  property="client_full_name" class="" title="Client Name" sortable="true" style="width:18%" />
                                <display:column  property="transaction_date_string" title="Transaction Date" class="center-align" sortable="true" style="width:15%">
                                </display:column>
                                <display:column  property="transaction_des" class="left-align" title="Description" sortable="true" style="width:15%" />
                                <display:column  property="transaction_recharge" class="right-align" title="Recharge" sortable="true" style="width:10%" />
                                <display:column  property="transaction_return" class="right-align" title="Return" sortable="true" style="width:10%" />
                                <display:column  property="transaction_receive" class="right-align" title="Receive" sortable="true" style="width:10%" />
                                <display:column  property="cur_balance" format="{0,number,0,000.0000}" class="right-align" title="Balance" sortable="true" style="width:10%" />
                            </display:table>
                        </div>    
                        <%
                            request.removeAttribute(Constants.USER_ID_LIST);
                            request.getSession(true).removeAttribute(Constants.MESSAGE);
                        %>
                    </div>
                    <div class="blank-height"></div>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>