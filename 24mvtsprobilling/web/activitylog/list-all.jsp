<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="java.util.ArrayList,activitylog.ActivityDTO" %>
<%@page import="activitylog.ActivityLoader,com.myapp.struts.util.Utils" %>
<%
    ArrayList<Integer> days = Utils.getDay();
    ArrayList<String> months = Utils.getMonth();
    ArrayList<Integer> years = Utils.getYear();
    String userId = request.getParameter("userId");
    if (userId == null) {
        userId = "";
    }
    String tableName = request.getParameter("tableName");
    if (tableName == null) {
        tableName = "";
    }
    String actionName = request.getParameter("actionName");
    if (actionName == null) {
        actionName = "";
    }
    
     String primaryKey = request.getParameter("primaryKey");
    if (primaryKey == null) {
        primaryKey = "";
    }


    String recordPerPage = (String) request.getParameter("recordPerPage");
    if (recordPerPage == null) {
        recordPerPage = "20";
    }
    String currentPage = (String) request.getParameter("d-49216-p");
    if (currentPage == null) {
        currentPage = "1";
    }

%>
<html  xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Client List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "activity");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right border_left">               
                <div class="pad_10 ">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";Activities");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="activitylog/listActivities.do" method="post">
                        <table class="search-table" border="0" cellpadding="0" cellspacing="1">
                            <tr class="odd">
                                <th>User ID/Client ID</th>
                                <td>
                                    <html:text property="userId" styleId="userId" value="<%=userId%>" />
                                </td>
                                <th>Table Name</th>
                                <td>
                                    <html:select property="tableName" style="width-154px" styleId="tableName">
                                        <html:option value="">Select</html:option>
                                        <%
                                            ArrayList<String> tables = ActivityLoader.getInstance().getTableNames();
                                            for (int i = 0; i < tables.size(); i++) {
                                                String table_name = tables.get(i);
                                        %>
                                        <html:option value="<%=table_name%>"><%=table_name%></html:option>
                                        <%}%>
                                    </html:select>
                                </td>
                                <th>Action Name</th>
                                <td>
                                    <html:select property="actionName" styleClass="width-154px" styleId="actionName">
                                        <html:option value="">Select</html:option>
                                        <html:option value="add">Add</html:option>
                                        <html:option value="edit">Edit</html:option>
                                        <html:option value="delete">Delete</html:option>
                                    </html:select>
                                </td>
                            </tr>
                            <tr class="even">
                                <th>Log Date(From)</th>
                                <td>
                                    <html:select property="fromDay" style="width:40px" styleId="fromDay">
                                        <%
                                            for (int i = 0; i < days.size(); i++) {
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=increment%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="fromMonth" style="width:75px" styleId="fromMonth">
                                        <%
                                            for (int i = 0; i < months.size(); i++) {
                                                String month = months.get(i);
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="fromYear" style="width:55px" styleId="fromYear">
                                        <%
                                            for (int i = 0; i < years.size(); i++) {
                                                String year = String.valueOf(years.get(i));
                                        %>
                                        <html:option value="<%=year%>"><%=year%></html:option>
                                        <%}%>
                                    </html:select>
                                </td>
                                <th>Log Date(To)</th>
                                <td>
                                    <html:select property="toDay" style="width:40px" styleId="toDay">
                                        <%
                                            for (int i = 0; i < days.size(); i++) {
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=increment%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="toMonth" style="width:75px" styleId="toMonth">
                                        <%
                                            for (int i = 0; i < months.size(); i++) {
                                                String month = months.get(i);
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="toYear" style="width:55px" styleId="toYear">
                                        <%
                                            for (int i = 0; i < years.size(); i++) {
                                                String year = String.valueOf(years.get(i));
                                        %>
                                        <html:option value="<%=year%>"><%=year%></html:option>
                                        <%}%>
                                    </html:select>
                                </td>
                                <th>Primary Key</th>
                                <td>
                                    <input type="text" name="primaryKey" value="<%=primaryKey%>" />
                                </td>
                            </tr>
                            <tr class="odd">
                                <th>Record Per Page</th>
                                <td>
                                    <input type="text" name="recordPerPage" value="<%=recordPerPage%>" />
                                </td>
                                <th>Go To Page No.</th>
                                <td colspan="3">
                                    <input type="text" name="d-49216-p" value="<%=currentPage%>" />
                                </td>
                            </tr>
                            <tr class="even">
                                <td colspan="6">
                                    <div class="full-div">
                                        <div class="half-div float-center center-align">
                                            <html:submit styleClass="search-button" property="doSearch" value="Search" />
                                            <html:reset styleClass="search-button" value="Reset" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </html:form>
                    <div class="over_flow_content display_tag_content" align="center">
                        <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.ActivityForm.activityDTOs" pagesize="<%=Integer.parseInt(recordPerPage)%>" >
                            <display:setProperty name="paging.banner.item_name" value="Activity" />
                            <display:setProperty name="paging.banner.items_name" value="Activities" />
                            <display:column title="Primary Key" headerClass="center-align" class="left-align" sortable="true">
                                <a href="getDetailLog.do?cid=${data.id}&tname=${data.tableName}&pkey=${data.primaryKey}">${data.primaryKey}</a>
                            </display:column>
                            <display:column property="actionName" title="Action" sortable="true" headerClass="center-align" class="left-align"/>
                            <display:column property="tableName" title="Table Name" sortable="true" headerClass="center-align" class="left-align"/>                            
                            <display:column property="userId" title="Changed By" sortable="true" headerClass="center-align" class="left-align"/>
                            <display:column property="logTime" decorator="implement.displaytag.LongDateWrapper" title="Changed Time" sortable="true" headerClass="center-align" class="center-align"/>
                        </display:table>
                        <div class="height-5px"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>  
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>