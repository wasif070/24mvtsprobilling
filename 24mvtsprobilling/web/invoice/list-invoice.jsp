<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Invoice List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "invoice");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="viewPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.VIEW])%>"></c:set>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("invoice/listInvoice.do?list_all=1;Invoice");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/invoice/listInvoice.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Invoice Number</th>
                                    <td><html:text property="id" /></td>
                                    <th>Client ID</th>
                                    <td>
                                        <html:text property="client" />
                                    </td>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" style="width:50px" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" style="width:50px"  name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                    <td>
                                        <html:submit styleClass="custom-button" value="Search" />
                                        <html:reset styleClass="custom-button" value="Reset" />
                                    </td>
                                </tr>                               
                            </table>
                        </div>
                    </html:form>                   
                    <html:form action="/invoice/deleteInvoice" method="post" onsubmit="return confirm('Are you sure to take action?');" >
                        <div class="over_flow_content display_tag_content" align="center">                                
                            <div class="jerror_messge"><%=msg%></div> 
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.InvoiceForm.invoicelist"  pagesize="<%=Integer.parseInt(String.valueOf(recordPerPage))%>" >
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='One Dial Plan' />" style="width:5%">
                                    <input type="checkbox" name="ids" value="${data.id}" class="select_id"/>
                                </display:column>
                                <display:column  title="Invoice Id"  sortable="true"  style="width:15%" >${data.id} </display:column>
                                <display:column   class="" title="Client" sortable="true" style="width:18%">${data.client}</display:column>
                                <display:column   class="" title="Client Name" sortable="true" style="width:18%">${data.clientName}</display:column>
                                <display:column   class="left-align" title="From Date" sortable="true" style="width:18%">${data.startDate}</display:column>
                                <display:column   class="center-align" title="To Date" sortable="true" style="width:10%">${data.endDate} </display:column>
                                <display:column   class="center-align" title="Invoice Date" sortable="true" style="width:10%" >${data.invoiceDate}</display:column>
                                <display:column  class="center-align" title="Task" style="width:10%;" >
                                    <c:choose>
                                        <c:when test="${viewPermission==1}">
                                            <a href="../invoice/invoiceDetail.do?id=${data.id}" class="detail"  title="Details"></a>
                                        </c:when>
                                    </c:choose>
                                    <a href="../invoice/download.do?id=${data.id}" class="download" title="Download"></a>
                                </display:column>
                            </display:table>
                            <%
                            request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                        </div>
                        <div class="button_area" align="center">
                            <html:submit property="deleteBtn" styleClass="custom-button jmultipebtn" value="Delete Selected" />
                        </div>
                        <div class="blank-height"></div>
                    </html:form>  
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>