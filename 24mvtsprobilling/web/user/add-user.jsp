<%@page import="role.RoleDTO"%>
<%@page import="role.RoleLoader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Add User</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "user");
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("user/listUser.do?list_all=1;User");
                        navList.add(";Add User");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/user/addUser" method="post">                
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Add User</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center" valign="bottom">
                                            <bean:write name="UserForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">User ID</th>
                                        <td valign="top">
                                            <html:text property="userId" /><br/>
                                            <html:messages id="userId" property="userId">
                                                <bean:write name="userId"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Password</th>
                                        <td valign="top">
                                            <html:password property="userPassword" /><br/>
                                            <html:messages id="userPassword" property="userPassword">
                                                <bean:write name="userPassword"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Retype Password</th>
                                        <td valign="top">
                                            <html:password property="retypePassword" /><br/>
                                            <html:messages id="retypePassword" property="retypePassword">
                                                <bean:write name="retypePassword"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Full Name</th>
                                        <td valign="top">
                                            <html:text property="fullName" /><br/>
                                            <html:messages id="fullName" property="fullName">
                                                <bean:write name="fullName"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Role</th>
                                        <td valign="top">                                            
                                            <html:select property="userRoleId">
                                                <html:option value="0">--Select--</html:option>
                                                <%
                                                    ArrayList<RoleDTO> list = RoleLoader.getInstance().getRoleDTOs();
                                                    for (RoleDTO roleDTO : list) {
                                                %>
                                                <html:option value="<%=String.valueOf(roleDTO.getId())%>"><%=roleDTO.getRoleName()%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="userRoleId" property="userRoleId">
                                                <bean:write name="userRoleId"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Status</th>
                                        <td valign="top">
                                            <html:select property="userStatus">
                                                <%
                                                    for (int i = 0; i < Constants.USER_STATUS_VALUE.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.USER_STATUS_VALUE[i]%>"><%=Constants.USER_STATUS_STRING[i]%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="userStatus" property="userStatus">
                                                <bean:write name="userStatus"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <input type="hidden" name="searchLink" value="nai" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>