<%@page import="com.myapp.struts.settings.SettingsForm"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.settings.SettingsLoader,com.myapp.struts.settings.SettingsDTO,java.text.NumberFormat,java.text.DecimalFormat" %>

<%
    ArrayList<Integer> hours = Utils.getTimeValue(24);
    ArrayList<Integer> min = Utils.getTimeValue(60);
    ArrayList<Integer> sec = Utils.getTimeValue(60);
    NumberFormat formatter = new DecimalFormat("00");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue()%> :: Edit Settings</title>
        <div><%@include file="../includes/header.jsp"%></div>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "settings");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right" style="width:80%">            
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("settings/listSettings.do?list_all=1;Settings");
                        navList.add(";Edit Settings");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/settings/editSettings" method="post">
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Edit Settings</legend>
                            <table class="input_table" style="width:700px;" cellspacing="0" cellpadding="0" >
                                <tbody>                  
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="SettingsForm" property="message" filter="false"/>
                                        </td>
                                    </tr>   
                                    <%
                                        int id = Integer.parseInt(request.getParameter("id"));
                                        SettingsDTO s_dto = SettingsLoader.getInstance().getSettingsDTOById(id);
                                        if (s_dto.getEdit_style().equalsIgnoreCase("TIME")) {%>     
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td colspan="3" class="selopt"><div class="fl_left">Sign</div><div class="fl_left month">Hour</div><div class="fl_left">Min</div><div class="fl_left">Sec</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Time Configuration <span class="req_mark">*</span></th>
                                        <td colspan="3" class="selopt">
                                            <html:select property="sign" styleClass="sign">
                                                <html:option value="1">+</html:option>
                                                <html:option value="2">-</html:option>
                                            </html:select>
                                            <html:select property="hour" styleClass="hour">
                                                <%
                                                    for (int i = 0; i < hours.size(); i++) {
                                                        String increment = String.valueOf(i);
                                                        String temp = formatter.format((i));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="min" styleClass="min">
                                                <%
                                                    for (int i = 0; i < min.size(); i++) {
                                                        String increment = String.valueOf(i);
                                                        String temp = formatter.format((i));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="sec" styleClass="sec">
                                                <%
                                                    for (int i = 0; i < sec.size(); i++) {
                                                        String increment = String.valueOf(i);
                                                        String temp = formatter.format((i));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <%} else if (s_dto.getEdit_style().equalsIgnoreCase("DROP_DOWN")) {%>
                                    <tr>
                                        <th><%=s_dto.getSettingName()%> <span class="req_mark">*</span></th>
                                        <td colspan="3" class="selopt">
                                            <html:select property="settingValue" styleClass="settingValue">
                                                <%
                                                    for (int i = 1; i < 9; i++) {
                                                        String increment = String.valueOf(i);
                                                        String temp = formatter.format((i));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <%} else if (s_dto.getEdit_style().equalsIgnoreCase("YES_NO")) {%>
                                    <tr>
                                        <th><%=s_dto.getSettingName()%> <span class="req_mark">*</span></th>
                                        <td colspan="3" class="selopt">
                                            <html:select property="settingValue" styleClass="settingValue">                                                
                                                <html:option value="0">No</html:option>
                                                <html:option value="1">Yes</html:option>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <%} else if (s_dto.getEdit_style().equalsIgnoreCase("TXT")) {%>
                                    <tr>
                                        <th><%=s_dto.getSettingName()%> <span class="req_mark">*</span></th>
                                        <td colspan="3" class="selopt">
                                            <html:text property="settingValue" styleClass="settingValue" />                                                                                        
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr> 
                                        <th>&nbsp;</th>
                                        <td>
                                            <html:hidden property="id" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" class="custom-button" onclick="javascript:return confirm('Are you sure to want Update the Settings?');" value="Update" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>
    </body>
</html>